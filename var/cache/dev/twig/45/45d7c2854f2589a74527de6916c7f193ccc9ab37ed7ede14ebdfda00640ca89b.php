<?php

/* default/index.html.twig */
class __TwigTemplate_558dd231e55a22dc88d81c40d3d6bcf98ab42b06e1a0827860909c2cdab65654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b13038edd848580ba2b4b48bc540675b2713a7986d0f2d82bdc8489806ed528e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b13038edd848580ba2b4b48bc540675b2713a7986d0f2d82bdc8489806ed528e->enter($__internal_b13038edd848580ba2b4b48bc540675b2713a7986d0f2d82bdc8489806ed528e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_d6e35873c6b7d7df248a39160db1c8bc4aaa77b234f91a4e672a2e51d1847e37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6e35873c6b7d7df248a39160db1c8bc4aaa77b234f91a4e672a2e51d1847e37->enter($__internal_d6e35873c6b7d7df248a39160db1c8bc4aaa77b234f91a4e672a2e51d1847e37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
  <head>
    <!-- Required meta tags -->
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <!-- Bootstrap CSS -->
    <link href=\"https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6\" crossorigin=\"anonymous\">

    <title>Tienda Virtual</title>
    <link rel=\"manifest\" href=\"https://getbootstrap.com/docs/5.0/assets/img/favicons/manifest.json\">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <link href=\"https://getbootstrap.com/docs/5.0/examples/cover/cover.css\" rel=\"stylesheet\">
  </head>

  <style>
    body  {
           background-image: url(\"img/ropa_fondo.jpg\");
          }
  </style>

  <body class=\"d-flex h-100 text-center text-white bg-dark\">

    <div class=\"cover-container d-flex w-100 h-100 p-3 mx-auto flex-column\">
    <header class=\"mb-auto\">
      <div>
        <h3 class=\"float-md-start mb-0\">Tienda Virtual</h3>
        <nav class=\"nav nav-masthead justify-content-center float-md-end\">
          <a class=\"nav-link active\" aria-current=\"page\" href=\"#\">Inicio</a>
          <a class=\"nav-link\" href=\"/products\">Productos</a>
          <a class=\"nav-link\" href=\"/login\">Login</a>
          <a class=\"nav-link\" href=\"/register\">Registrarse</a>
        </nav>
      </div>
    </header>

    <main class=\"px-3\">
      <h1>Tienda Virtual de Ropa</h1>
      <p class=\"lead\">En esta tienda podes encontrar ropa de la mejor calidad y con los mejores precios.</p>
      <p class=\"lead\">
        <a href=\"/products\" class=\"btn btn-lg btn-secondary fw-bold border-white bg-white\">Ver productos</a>
      </p>
    </main>

    <footer class=\"mt-auto text-white-50\">
      <p>Plantilla de <a href=\"https://getbootstrap.com/\" class=\"text-white\">Bootstrap</a>.</p>
    </footer>
    </div>
  </body>
</html>";
        
        $__internal_b13038edd848580ba2b4b48bc540675b2713a7986d0f2d82bdc8489806ed528e->leave($__internal_b13038edd848580ba2b4b48bc540675b2713a7986d0f2d82bdc8489806ed528e_prof);

        
        $__internal_d6e35873c6b7d7df248a39160db1c8bc4aaa77b234f91a4e672a2e51d1847e37->leave($__internal_d6e35873c6b7d7df248a39160db1c8bc4aaa77b234f91a4e672a2e51d1847e37_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html lang=\"en\">
  <head>
    <!-- Required meta tags -->
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <!-- Bootstrap CSS -->
    <link href=\"https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6\" crossorigin=\"anonymous\">

    <title>Tienda Virtual</title>
    <link rel=\"manifest\" href=\"https://getbootstrap.com/docs/5.0/assets/img/favicons/manifest.json\">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <link href=\"https://getbootstrap.com/docs/5.0/examples/cover/cover.css\" rel=\"stylesheet\">
  </head>

  <style>
    body  {
           background-image: url(\"img/ropa_fondo.jpg\");
          }
  </style>

  <body class=\"d-flex h-100 text-center text-white bg-dark\">

    <div class=\"cover-container d-flex w-100 h-100 p-3 mx-auto flex-column\">
    <header class=\"mb-auto\">
      <div>
        <h3 class=\"float-md-start mb-0\">Tienda Virtual</h3>
        <nav class=\"nav nav-masthead justify-content-center float-md-end\">
          <a class=\"nav-link active\" aria-current=\"page\" href=\"#\">Inicio</a>
          <a class=\"nav-link\" href=\"/products\">Productos</a>
          <a class=\"nav-link\" href=\"/login\">Login</a>
          <a class=\"nav-link\" href=\"/register\">Registrarse</a>
        </nav>
      </div>
    </header>

    <main class=\"px-3\">
      <h1>Tienda Virtual de Ropa</h1>
      <p class=\"lead\">En esta tienda podes encontrar ropa de la mejor calidad y con los mejores precios.</p>
      <p class=\"lead\">
        <a href=\"/products\" class=\"btn btn-lg btn-secondary fw-bold border-white bg-white\">Ver productos</a>
      </p>
    </main>

    <footer class=\"mt-auto text-white-50\">
      <p>Plantilla de <a href=\"https://getbootstrap.com/\" class=\"text-white\">Bootstrap</a>.</p>
    </footer>
    </div>
  </body>
</html>", "default/index.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/default/index.html.twig");
    }
}
