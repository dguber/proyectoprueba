<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_6c95da31b0e57a619c4ff723f2700fad63b24fcd60462b804bfcfcb4440f592b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db70ecfc6eb85a28dae4483c124344e2bd5f3d2aa3993ef97bce8b4e3627a6b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db70ecfc6eb85a28dae4483c124344e2bd5f3d2aa3993ef97bce8b4e3627a6b5->enter($__internal_db70ecfc6eb85a28dae4483c124344e2bd5f3d2aa3993ef97bce8b4e3627a6b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_8e30be8dd42f1f40d5d7e9497c47ee1bb497e42eeac21f2022f3acd5a2c3f3c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e30be8dd42f1f40d5d7e9497c47ee1bb497e42eeac21f2022f3acd5a2c3f3c6->enter($__internal_8e30be8dd42f1f40d5d7e9497c47ee1bb497e42eeac21f2022f3acd5a2c3f3c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_db70ecfc6eb85a28dae4483c124344e2bd5f3d2aa3993ef97bce8b4e3627a6b5->leave($__internal_db70ecfc6eb85a28dae4483c124344e2bd5f3d2aa3993ef97bce8b4e3627a6b5_prof);

        
        $__internal_8e30be8dd42f1f40d5d7e9497c47ee1bb497e42eeac21f2022f3acd5a2c3f3c6->leave($__internal_8e30be8dd42f1f40d5d7e9497c47ee1bb497e42eeac21f2022f3acd5a2c3f3c6_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_1fbfa7cc3b5d2241b98e5a8cc01a44fc17a674bdb10814136def9f6a15f4e5d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fbfa7cc3b5d2241b98e5a8cc01a44fc17a674bdb10814136def9f6a15f4e5d6->enter($__internal_1fbfa7cc3b5d2241b98e5a8cc01a44fc17a674bdb10814136def9f6a15f4e5d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_ed56092f5bd1ab855b050f7c98ce6619d198d0ee1c5578ada98f38a85b94f364 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed56092f5bd1ab855b050f7c98ce6619d198d0ee1c5578ada98f38a85b94f364->enter($__internal_ed56092f5bd1ab855b050f7c98ce6619d198d0ee1c5578ada98f38a85b94f364_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_ed56092f5bd1ab855b050f7c98ce6619d198d0ee1c5578ada98f38a85b94f364->leave($__internal_ed56092f5bd1ab855b050f7c98ce6619d198d0ee1c5578ada98f38a85b94f364_prof);

        
        $__internal_1fbfa7cc3b5d2241b98e5a8cc01a44fc17a674bdb10814136def9f6a15f4e5d6->leave($__internal_1fbfa7cc3b5d2241b98e5a8cc01a44fc17a674bdb10814136def9f6a15f4e5d6_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/usuario/Documentos/www/proyectoprueba/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
