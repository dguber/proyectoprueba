<?php

/* users/edit.html.twig */
class __TwigTemplate_92029faebd43e1d6718ef63725fc3c6d2dc808dfeba571741f5e226b3a52e097 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbd9fe7a7dd26518cf9912279f2195d9c49c9973cb664227028c1cb6d86cc3d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbd9fe7a7dd26518cf9912279f2195d9c49c9973cb664227028c1cb6d86cc3d9->enter($__internal_bbd9fe7a7dd26518cf9912279f2195d9c49c9973cb664227028c1cb6d86cc3d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/edit.html.twig"));

        $__internal_b6d10416d3a1bffdfce165ad90b7f07c0646af6e766d1da0086e79934c681bb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6d10416d3a1bffdfce165ad90b7f07c0646af6e766d1da0086e79934c681bb9->enter($__internal_b6d10416d3a1bffdfce165ad90b7f07c0646af6e766d1da0086e79934c681bb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bbd9fe7a7dd26518cf9912279f2195d9c49c9973cb664227028c1cb6d86cc3d9->leave($__internal_bbd9fe7a7dd26518cf9912279f2195d9c49c9973cb664227028c1cb6d86cc3d9_prof);

        
        $__internal_b6d10416d3a1bffdfce165ad90b7f07c0646af6e766d1da0086e79934c681bb9->leave($__internal_b6d10416d3a1bffdfce165ad90b7f07c0646af6e766d1da0086e79934c681bb9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_114c1eb26a97c14790527df567fbd6604e0cbbd4a90c3305b66c1a8006d7aa84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_114c1eb26a97c14790527df567fbd6604e0cbbd4a90c3305b66c1a8006d7aa84->enter($__internal_114c1eb26a97c14790527df567fbd6604e0cbbd4a90c3305b66c1a8006d7aa84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ad8ac1a1724293171e82eb88dd5fbe6e7f767e68d4322a0ccd7389aa67145145 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad8ac1a1724293171e82eb88dd5fbe6e7f767e68d4322a0ccd7389aa67145145->enter($__internal_ad8ac1a1724293171e82eb88dd5fbe6e7f767e68d4322a0ccd7389aa67145145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> User edit</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

          </div>
        </div>
      </div>
      ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
    </main>
";
        
        $__internal_ad8ac1a1724293171e82eb88dd5fbe6e7f767e68d4322a0ccd7389aa67145145->leave($__internal_ad8ac1a1724293171e82eb88dd5fbe6e7f767e68d4322a0ccd7389aa67145145_prof);

        
        $__internal_114c1eb26a97c14790527df567fbd6604e0cbbd4a90c3305b66c1a8006d7aa84->leave($__internal_114c1eb26a97c14790527df567fbd6604e0cbbd4a90c3305b66c1a8006d7aa84_prof);

    }

    public function getTemplateName()
    {
        return "users/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  86 => 27,  78 => 22,  73 => 20,  69 => 19,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> User edit</h1>
          <a href=\"{{ path('users_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(edit_form) }}
                {{ form_widget(edit_form) }}
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            {{ form_end(edit_form) }}

          </div>
        </div>
      </div>
      {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      {{ form_end(delete_form) }}
    </main>
{% endblock %}
", "users/edit.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/users/edit.html.twig");
    }
}
