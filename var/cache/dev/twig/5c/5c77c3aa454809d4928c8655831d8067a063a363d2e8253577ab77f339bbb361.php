<?php

/* products/sshow.html.twig */
class __TwigTemplate_eb28575a77dee22d6445d2c036716a35a310d8ecae8de5d8e8bc34bd0dd0292d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/sshow.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cbdd1a4f570e5d1ebf8e88fbd298207bac7aa2daf3f2309cd783026f552211d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cbdd1a4f570e5d1ebf8e88fbd298207bac7aa2daf3f2309cd783026f552211d->enter($__internal_0cbdd1a4f570e5d1ebf8e88fbd298207bac7aa2daf3f2309cd783026f552211d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/sshow.html.twig"));

        $__internal_6906edc60debb17c9ee2a9747af5345e103cffc38dbe015a6369db3f685d98b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6906edc60debb17c9ee2a9747af5345e103cffc38dbe015a6369db3f685d98b9->enter($__internal_6906edc60debb17c9ee2a9747af5345e103cffc38dbe015a6369db3f685d98b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/sshow.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0cbdd1a4f570e5d1ebf8e88fbd298207bac7aa2daf3f2309cd783026f552211d->leave($__internal_0cbdd1a4f570e5d1ebf8e88fbd298207bac7aa2daf3f2309cd783026f552211d_prof);

        
        $__internal_6906edc60debb17c9ee2a9747af5345e103cffc38dbe015a6369db3f685d98b9->leave($__internal_6906edc60debb17c9ee2a9747af5345e103cffc38dbe015a6369db3f685d98b9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_036043c6a282f646c2a7a0a809c522d964342822a9ae8bf6bdb4b0e4fe2f6a85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_036043c6a282f646c2a7a0a809c522d964342822a9ae8bf6bdb4b0e4fe2f6a85->enter($__internal_036043c6a282f646c2a7a0a809c522d964342822a9ae8bf6bdb4b0e4fe2f6a85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f3933eff557b6fde9dd64ba19f57e481ec76fe3449043028afdb636fc85adebe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3933eff557b6fde9dd64ba19f57e481ec76fe3449043028afdb636fc85adebe->enter($__internal_f3933eff557b6fde9dd64ba19f57e481ec76fe3449043028afdb636fc85adebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "name", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Category</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "category", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Image</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/products/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "image", array()), "html", null, true);
        echo "</td>
                <img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/products/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "image", array()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "name", array()), "html", null, true);
        echo "\" width=\"100\" height=\"100\">
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("searchproducts"), "html", null, true);
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_f3933eff557b6fde9dd64ba19f57e481ec76fe3449043028afdb636fc85adebe->leave($__internal_f3933eff557b6fde9dd64ba19f57e481ec76fe3449043028afdb636fc85adebe_prof);

        
        $__internal_036043c6a282f646c2a7a0a809c522d964342822a9ae8bf6bdb4b0e4fe2f6a85->leave($__internal_036043c6a282f646c2a7a0a809c522d964342822a9ae8bf6bdb4b0e4fe2f6a85_prof);

    }

    public function getTemplateName()
    {
        return "products/sshow.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 30,  83 => 23,  78 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ product.id }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ product.name }}</td>
            </tr>
            <tr>
                <th>Category</th>
                <td>{{ product.category }}</td>
            </tr>
            <tr>
                <th>Image</th>
                <td>{{asset('img/products/')}}{{ product.image }}</td>
                <img src=\"{{asset('img/products/')}}{{ product.image }}\" alt=\"{{ product.name }}\" width=\"100\" height=\"100\">
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{asset('searchproducts')}}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}", "products/sshow.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/products/sshow.html.twig");
    }
}
