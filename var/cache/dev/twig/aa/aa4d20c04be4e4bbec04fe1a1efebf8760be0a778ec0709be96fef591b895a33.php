<?php

/* categories/show.html.twig */
class __TwigTemplate_f069a06189935a29c68302b25d13f5df1eb69854a11035a60a53a09ed4922009 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categories/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f9729dab4e6f9e1e50968e7b22c595ceec651c16c3f14181e4a4c1d307f5372d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9729dab4e6f9e1e50968e7b22c595ceec651c16c3f14181e4a4c1d307f5372d->enter($__internal_f9729dab4e6f9e1e50968e7b22c595ceec651c16c3f14181e4a4c1d307f5372d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/show.html.twig"));

        $__internal_108ba2c4b7ba2da8139eac19ec62fed9a7685006c077febefca5762c042c1569 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_108ba2c4b7ba2da8139eac19ec62fed9a7685006c077febefca5762c042c1569->enter($__internal_108ba2c4b7ba2da8139eac19ec62fed9a7685006c077febefca5762c042c1569_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f9729dab4e6f9e1e50968e7b22c595ceec651c16c3f14181e4a4c1d307f5372d->leave($__internal_f9729dab4e6f9e1e50968e7b22c595ceec651c16c3f14181e4a4c1d307f5372d_prof);

        
        $__internal_108ba2c4b7ba2da8139eac19ec62fed9a7685006c077febefca5762c042c1569->leave($__internal_108ba2c4b7ba2da8139eac19ec62fed9a7685006c077febefca5762c042c1569_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a11037a4c214e365000f9c56107f47f32c15c4bfe7aa8f2217bbbfbd8c5315c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a11037a4c214e365000f9c56107f47f32c15c4bfe7aa8f2217bbbfbd8c5315c8->enter($__internal_a11037a4c214e365000f9c56107f47f32c15c4bfe7aa8f2217bbbfbd8c5315c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_de16319c2f5d11ec57e6e685eb6fcc1e821746a1ad2bc92c7877a86e280c69d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de16319c2f5d11ec57e6e685eb6fcc1e821746a1ad2bc92c7877a86e280c69d3->enter($__internal_de16319c2f5d11ec57e6e685eb6fcc1e821746a1ad2bc92c7877a86e280c69d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> Category</h1>
            <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["category"] ?? $this->getContext($context, "category")), "id", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["category"] ?? $this->getContext($context, "category")), "name", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    Image: ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute(($context["category"] ?? $this->getContext($context, "category")), "icon", array()), "html", null, true);
        echo "
                    <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/categories/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(($context["category"] ?? $this->getContext($context, "category")), "icon", array()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["category"] ?? $this->getContext($context, "category")), "icon", array()), "html", null, true);
        echo "\" width=\"100\" height=\"100\">
                </li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        <li>
                            <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_edit", array("id" => $this->getAttribute(($context["category"] ?? $this->getContext($context, "category")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
                        </li>
                        <li>
                            ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            ";
        // line 34
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
                        </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
";
        
        $__internal_de16319c2f5d11ec57e6e685eb6fcc1e821746a1ad2bc92c7877a86e280c69d3->leave($__internal_de16319c2f5d11ec57e6e685eb6fcc1e821746a1ad2bc92c7877a86e280c69d3_prof);

        
        $__internal_a11037a4c214e365000f9c56107f47f32c15c4bfe7aa8f2217bbbfbd8c5315c8->leave($__internal_a11037a4c214e365000f9c56107f47f32c15c4bfe7aa8f2217bbbfbd8c5315c8_prof);

    }

    public function getTemplateName()
    {
        return "categories/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 34,  100 => 32,  94 => 29,  83 => 24,  79 => 23,  74 => 21,  70 => 20,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> Category</h1>
            <a href=\"{{ path('categories_index') }}\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: {{ category.id }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: {{ category.name }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    Image: {{ category.icon }}
                    <img src=\"{{asset('img/categories/')}}{{ category.icon }}\" alt=\"{{ category.icon }}\" width=\"100\" height=\"100\">
                </li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        <li>
                            <a href=\"{{ path('categories_edit', { 'id': category.id }) }}\">Edit</a>
                        </li>
                        <li>
                            {{ form_start(delete_form) }}
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            {{ form_end(delete_form) }}
                        </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
{% endblock %}
", "categories/show.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/categories/show.html.twig");
    }
}
