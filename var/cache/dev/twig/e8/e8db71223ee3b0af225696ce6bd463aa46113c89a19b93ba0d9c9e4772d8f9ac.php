<?php

/* products/show.html.twig */
class __TwigTemplate_9632f305b021da850c96e94384cb1d4cf6475362d40fc38c637ef41a9ceefe79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a251fb1d21b6ca014b53ef883149d38cd42f5a3d2dd8400f1d0a13231af6a64d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a251fb1d21b6ca014b53ef883149d38cd42f5a3d2dd8400f1d0a13231af6a64d->enter($__internal_a251fb1d21b6ca014b53ef883149d38cd42f5a3d2dd8400f1d0a13231af6a64d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/show.html.twig"));

        $__internal_2a6ced11c476d755424753e4e4b24b908fed7083a29c18e4030b87c438592108 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a6ced11c476d755424753e4e4b24b908fed7083a29c18e4030b87c438592108->enter($__internal_2a6ced11c476d755424753e4e4b24b908fed7083a29c18e4030b87c438592108_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a251fb1d21b6ca014b53ef883149d38cd42f5a3d2dd8400f1d0a13231af6a64d->leave($__internal_a251fb1d21b6ca014b53ef883149d38cd42f5a3d2dd8400f1d0a13231af6a64d_prof);

        
        $__internal_2a6ced11c476d755424753e4e4b24b908fed7083a29c18e4030b87c438592108->leave($__internal_2a6ced11c476d755424753e4e4b24b908fed7083a29c18e4030b87c438592108_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_93285636b9c78e9cdb3629d2e595abdde63f3413232ddbda2e45c00d83b3431b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93285636b9c78e9cdb3629d2e595abdde63f3413232ddbda2e45c00d83b3431b->enter($__internal_93285636b9c78e9cdb3629d2e595abdde63f3413232ddbda2e45c00d83b3431b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1e724c26754c5a4f139c9dafb78eb53598732949824c1bd4acb4dfbeccc4e1bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e724c26754c5a4f139c9dafb78eb53598732949824c1bd4acb4dfbeccc4e1bd->enter($__internal_1e724c26754c5a4f139c9dafb78eb53598732949824c1bd4acb4dfbeccc4e1bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> Product</h1>
            <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "id", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "name", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Category: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "subcategory", array()), "categoryId", array()), "name", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">SubCategory: ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "subcategory", array()), "name", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    Image: ";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/products/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "image", array()), "html", null, true);
        echo "
                    <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/products/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "image", array()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "name", array()), "html", null, true);
        echo "\" width=\"100\" height=\"100\">
                </li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Price: \$";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "price", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        ";
        // line 31
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 32
            echo "                        <li>
                            <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_edit", array("id" => $this->getAttribute(($context["product"] ?? $this->getContext($context, "product")), "id", array()))), "html", null, true);
            echo "\">Edit</a>
                        </li>
                        ";
        }
        // line 36
        echo "                        ";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 37
            echo "                        <li>
                            ";
            // line 38
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
            echo "
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            ";
            // line 40
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
            echo "
                        </li>
                        ";
        }
        // line 43
        echo "                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
";
        
        $__internal_1e724c26754c5a4f139c9dafb78eb53598732949824c1bd4acb4dfbeccc4e1bd->leave($__internal_1e724c26754c5a4f139c9dafb78eb53598732949824c1bd4acb4dfbeccc4e1bd_prof);

        
        $__internal_93285636b9c78e9cdb3629d2e595abdde63f3413232ddbda2e45c00d83b3431b->leave($__internal_93285636b9c78e9cdb3629d2e595abdde63f3413232ddbda2e45c00d83b3431b_prof);

    }

    public function getTemplateName()
    {
        return "products/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 43,  128 => 40,  123 => 38,  120 => 37,  117 => 36,  111 => 33,  108 => 32,  106 => 31,  100 => 28,  92 => 26,  87 => 25,  82 => 23,  78 => 22,  74 => 21,  70 => 20,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> Product</h1>
            <a href=\"{{ path('products_index') }}\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: {{ product.id }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: {{ product.name }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Category: {{ product.subcategory.categoryId.name }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">SubCategory: {{ product.subcategory.name }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    Image: {{asset('img/products/')}}{{ product.image }}
                    <img src=\"{{asset('img/products/')}}{{ product.image }}\" alt=\"{{ product.name }}\" width=\"100\" height=\"100\">
                </li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Price: \${{ product.price }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        {% if is_granted('ROLE_USER') %}
                        <li>
                            <a href=\"{{ path('products_edit', { 'id': product.id }) }}\">Edit</a>
                        </li>
                        {% endif %}
                        {% if is_granted('ROLE_USER') %}
                        <li>
                            {{ form_start(delete_form) }}
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            {{ form_end(delete_form) }}
                        </li>
                        {% endif %}
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
{% endblock %}

", "products/show.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/products/show.html.twig");
    }
}
