<?php

/* products/sindex.html.twig */
class __TwigTemplate_62bd3c72c621622281cfd9577c5601e395d45912153a59957b09966f97b90169 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/sindex.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b5fd276483cbdda94b9e9b79509d417def0692b3840ddcddcfb4b17761f79ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b5fd276483cbdda94b9e9b79509d417def0692b3840ddcddcfb4b17761f79ca->enter($__internal_5b5fd276483cbdda94b9e9b79509d417def0692b3840ddcddcfb4b17761f79ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/sindex.html.twig"));

        $__internal_b5a5192d55b0ce8ab11cf6aa9548517c5c008046430f46b2eac842144f18b3dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5a5192d55b0ce8ab11cf6aa9548517c5c008046430f46b2eac842144f18b3dd->enter($__internal_b5a5192d55b0ce8ab11cf6aa9548517c5c008046430f46b2eac842144f18b3dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/sindex.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b5fd276483cbdda94b9e9b79509d417def0692b3840ddcddcfb4b17761f79ca->leave($__internal_5b5fd276483cbdda94b9e9b79509d417def0692b3840ddcddcfb4b17761f79ca_prof);

        
        $__internal_b5a5192d55b0ce8ab11cf6aa9548517c5c008046430f46b2eac842144f18b3dd->leave($__internal_b5a5192d55b0ce8ab11cf6aa9548517c5c008046430f46b2eac842144f18b3dd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_bf031b1d924f50dca8003a597acfa57d33078a01fbe28bdad058c178d522e098 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf031b1d924f50dca8003a597acfa57d33078a01fbe28bdad058c178d522e098->enter($__internal_bf031b1d924f50dca8003a597acfa57d33078a01fbe28bdad058c178d522e098_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_154158dbaad20381ff42230301821ae0120e0d7583011cfe3acb463de92cd7a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_154158dbaad20381ff42230301821ae0120e0d7583011cfe3acb463de92cd7a4->enter($__internal_154158dbaad20381ff42230301821ae0120e0d7583011cfe3acb463de92cd7a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Products list</h1>
    ";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Search\" />
    ";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Category</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "category", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image", array()), "html", null, true);
            echo " <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/products/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "\" width=\"100\" height=\"100\"></td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("searchproducts/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">show</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>
";
        
        $__internal_154158dbaad20381ff42230301821ae0120e0d7583011cfe3acb463de92cd7a4->leave($__internal_154158dbaad20381ff42230301821ae0120e0d7583011cfe3acb463de92cd7a4_prof);

        
        $__internal_bf031b1d924f50dca8003a597acfa57d33078a01fbe28bdad058c178d522e098->leave($__internal_bf031b1d924f50dca8003a597acfa57d33078a01fbe28bdad058c178d522e098_prof);

    }

    public function getTemplateName()
    {
        return "products/sindex.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 35,  109 => 29,  97 => 25,  93 => 24,  89 => 23,  83 => 22,  80 => 21,  76 => 20,  61 => 8,  56 => 6,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Products list</h1>
    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Search\" />
    {{ form_end(form) }}
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Category</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for product in products %}
            <tr>
                <td><a href=\"{{ path('products_show', { 'id': product.id }) }}\">{{ product.id }}</a></td>
                <td>{{ product.name }}</td>
                <td>{{ product.category }}</td>
                <td>{{ product.image }} <img src=\"{{asset('img/products/')}}{{ product.image }}\" alt=\"{{ product.name }}\" width=\"100\" height=\"100\"></td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{asset('searchproducts/')}}{{ product.id }}\">show</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
", "products/sindex.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/products/sindex.html.twig");
    }
}
