<?php

/* categories/edit.html.twig */
class __TwigTemplate_3b39d8a115a03f5773118827976ef92791e7e10a9c21f5e7391bdf4c3eb6d7cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categories/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14bb7a013f6baff82fb3d41589c0de2aed89be16240bafaa686f3e771f3168c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14bb7a013f6baff82fb3d41589c0de2aed89be16240bafaa686f3e771f3168c3->enter($__internal_14bb7a013f6baff82fb3d41589c0de2aed89be16240bafaa686f3e771f3168c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/edit.html.twig"));

        $__internal_297853b9c5fbeb831912ed93d6c947213955795621eb69ec176872ac0b1e43a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_297853b9c5fbeb831912ed93d6c947213955795621eb69ec176872ac0b1e43a0->enter($__internal_297853b9c5fbeb831912ed93d6c947213955795621eb69ec176872ac0b1e43a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_14bb7a013f6baff82fb3d41589c0de2aed89be16240bafaa686f3e771f3168c3->leave($__internal_14bb7a013f6baff82fb3d41589c0de2aed89be16240bafaa686f3e771f3168c3_prof);

        
        $__internal_297853b9c5fbeb831912ed93d6c947213955795621eb69ec176872ac0b1e43a0->leave($__internal_297853b9c5fbeb831912ed93d6c947213955795621eb69ec176872ac0b1e43a0_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f867f9e5e2fc8d52a01baaf51cc08f6d4846d6815a4b9ddfaabfa99bb1af9ba1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f867f9e5e2fc8d52a01baaf51cc08f6d4846d6815a4b9ddfaabfa99bb1af9ba1->enter($__internal_f867f9e5e2fc8d52a01baaf51cc08f6d4846d6815a4b9ddfaabfa99bb1af9ba1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3b39f1abf021f7de6e7c977f23162ffcd487261506b38622e436b78d48bee495 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b39f1abf021f7de6e7c977f23162ffcd487261506b38622e436b78d48bee495->enter($__internal_3b39f1abf021f7de6e7c977f23162ffcd487261506b38622e436b78d48bee495_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Category edit</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

          </div>
        </div>
      </div>
      ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
    </main>
";
        
        $__internal_3b39f1abf021f7de6e7c977f23162ffcd487261506b38622e436b78d48bee495->leave($__internal_3b39f1abf021f7de6e7c977f23162ffcd487261506b38622e436b78d48bee495_prof);

        
        $__internal_f867f9e5e2fc8d52a01baaf51cc08f6d4846d6815a4b9ddfaabfa99bb1af9ba1->leave($__internal_f867f9e5e2fc8d52a01baaf51cc08f6d4846d6815a4b9ddfaabfa99bb1af9ba1_prof);

    }

    // line 33
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b1beb8f5c5619254969480d08fbb584a0048ee94322cc7657b69a16a83324f79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1beb8f5c5619254969480d08fbb584a0048ee94322cc7657b69a16a83324f79->enter($__internal_b1beb8f5c5619254969480d08fbb584a0048ee94322cc7657b69a16a83324f79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_ae5ef6f5e11edad105fb11add50fd3f75cd1204e94eca89ef8f9f85037f800fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae5ef6f5e11edad105fb11add50fd3f75cd1204e94eca89ef8f9f85037f800fd->enter($__internal_ae5ef6f5e11edad105fb11add50fd3f75cd1204e94eca89ef8f9f85037f800fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 34
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/newProductImg.js"), "html", null, true);
        echo "\">
";
        
        $__internal_ae5ef6f5e11edad105fb11add50fd3f75cd1204e94eca89ef8f9f85037f800fd->leave($__internal_ae5ef6f5e11edad105fb11add50fd3f75cd1204e94eca89ef8f9f85037f800fd_prof);

        
        $__internal_b1beb8f5c5619254969480d08fbb584a0048ee94322cc7657b69a16a83324f79->leave($__internal_b1beb8f5c5619254969480d08fbb584a0048ee94322cc7657b69a16a83324f79_prof);

    }

    public function getTemplateName()
    {
        return "categories/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 34,  105 => 33,  92 => 29,  87 => 27,  79 => 22,  74 => 20,  70 => 19,  56 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Category edit</h1>
          <a href=\"{{ path('categories_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(edit_form) }}
                {{ form_widget(edit_form) }}
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            {{ form_end(edit_form) }}

          </div>
        </div>
      </div>
      {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      {{ form_end(delete_form) }}
    </main>
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\" src=\"{{asset('js/newProductImg.js')}}\">
{% endblock %}

", "categories/edit.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/categories/edit.html.twig");
    }
}
