<?php

/* users/index.html.twig */
class __TwigTemplate_fca8c10c9dabd16a3230c1eeafa7250c222019327c6f555fa575e08412856096 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc2b431ddb7c1ced851da9f6ced72d7dc39c81eb292197dada3f30cae8351045 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc2b431ddb7c1ced851da9f6ced72d7dc39c81eb292197dada3f30cae8351045->enter($__internal_cc2b431ddb7c1ced851da9f6ced72d7dc39c81eb292197dada3f30cae8351045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/index.html.twig"));

        $__internal_ac3992828e2ebd71e296be004954772cbd0fcab798291992d0741ef01ba44f15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac3992828e2ebd71e296be004954772cbd0fcab798291992d0741ef01ba44f15->enter($__internal_ac3992828e2ebd71e296be004954772cbd0fcab798291992d0741ef01ba44f15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cc2b431ddb7c1ced851da9f6ced72d7dc39c81eb292197dada3f30cae8351045->leave($__internal_cc2b431ddb7c1ced851da9f6ced72d7dc39c81eb292197dada3f30cae8351045_prof);

        
        $__internal_ac3992828e2ebd71e296be004954772cbd0fcab798291992d0741ef01ba44f15->leave($__internal_ac3992828e2ebd71e296be004954772cbd0fcab798291992d0741ef01ba44f15_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a96b9f91ff754b99c2c281e0432a07e77e40ad1f1f43468a5e942933c8842b22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a96b9f91ff754b99c2c281e0432a07e77e40ad1f1f43468a5e942933c8842b22->enter($__internal_a96b9f91ff754b99c2c281e0432a07e77e40ad1f1f43468a5e942933c8842b22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_75fd1b03edf3a7ea5cf0a9eb3d1b175429a9870577ac9b2e4bfc65b566625f7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75fd1b03edf3a7ea5cf0a9eb3d1b175429a9870577ac9b2e4bfc65b566625f7c->enter($__internal_75fd1b03edf3a7ea5cf0a9eb3d1b175429a9870577ac9b2e4bfc65b566625f7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> User List</h1>
            <li>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_new");
        echo "\">Create a new user</a>
            </li>
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Image</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 35
            echo "                        <tr>
                            <td><a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</a></td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "password", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">show</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_edit", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_75fd1b03edf3a7ea5cf0a9eb3d1b175429a9870577ac9b2e4bfc65b566625f7c->leave($__internal_75fd1b03edf3a7ea5cf0a9eb3d1b175429a9870577ac9b2e4bfc65b566625f7c_prof);

        
        $__internal_a96b9f91ff754b99c2c281e0432a07e77e40ad1f1f43468a5e942933c8842b22->leave($__internal_a96b9f91ff754b99c2c281e0432a07e77e40ad1f1f43468a5e942933c8842b22_prof);

    }

    public function getTemplateName()
    {
        return "users/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 52,  118 => 46,  112 => 43,  105 => 39,  101 => 38,  97 => 37,  91 => 36,  88 => 35,  84 => 34,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> User List</h1>
            <li>
                <a href=\"{{ path('users_new') }}\">Create a new user</a>
            </li>
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Image</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for user in users %}
                        <tr>
                            <td><a href=\"{{ path('users_show', { 'id': user.id }) }}\">{{ user.id }}</a></td>
                            <td>{{ user.username }}</td>
                            <td>{{ user.password }}</td>
                            <td>{{ user.email }}</td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"{{ path('users_show', { 'id': user.id }) }}\">show</a>
                                    </li>
                                    <li>
                                        <a href=\"{{ path('users_edit', { 'id': user.id }) }}\">edit</a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    {% endfor %}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
{% endblock %}
", "users/index.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/users/index.html.twig");
    }
}
