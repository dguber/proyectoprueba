<?php

/* subcategory/index.html.twig */
class __TwigTemplate_4e89d894f8e84ee24cf03c29d7fa96d12a87b302df9c6d19a7c0376ac0b8b18f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "subcategory/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b51128dbb05395b370cb37fc24bbe2e0df80e9641854341ca370a62acdbee09f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b51128dbb05395b370cb37fc24bbe2e0df80e9641854341ca370a62acdbee09f->enter($__internal_b51128dbb05395b370cb37fc24bbe2e0df80e9641854341ca370a62acdbee09f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/index.html.twig"));

        $__internal_02eb80bc092b43b0bb571b2dbfa4f6cd4288fd58f66dedce4e77421172a038cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02eb80bc092b43b0bb571b2dbfa4f6cd4288fd58f66dedce4e77421172a038cd->enter($__internal_02eb80bc092b43b0bb571b2dbfa4f6cd4288fd58f66dedce4e77421172a038cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b51128dbb05395b370cb37fc24bbe2e0df80e9641854341ca370a62acdbee09f->leave($__internal_b51128dbb05395b370cb37fc24bbe2e0df80e9641854341ca370a62acdbee09f_prof);

        
        $__internal_02eb80bc092b43b0bb571b2dbfa4f6cd4288fd58f66dedce4e77421172a038cd->leave($__internal_02eb80bc092b43b0bb571b2dbfa4f6cd4288fd58f66dedce4e77421172a038cd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_01691d363870d0ed08257a957aab97755cfc106d2e5c21c1e4f4599301b61c98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01691d363870d0ed08257a957aab97755cfc106d2e5c21c1e4f4599301b61c98->enter($__internal_01691d363870d0ed08257a957aab97755cfc106d2e5c21c1e4f4599301b61c98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_47fe90fb200ac2fad0624176b97fb8da42d3ca542a92228f9d4f0cd51f53ea28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47fe90fb200ac2fad0624176b97fb8da42d3ca542a92228f9d4f0cd51f53ea28->enter($__internal_47fe90fb200ac2fad0624176b97fb8da42d3ca542a92228f9d4f0cd51f53ea28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> Sub-Categories List</h1>
            <li>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_new");
        echo "\">Create a new subcategory</a>
            </li>
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Icon</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["subcategories"] ?? $this->getContext($context, "subcategories")));
        foreach ($context['_seq'] as $context["_key"] => $context["subcategory"]) {
            // line 34
            echo "                        <tr>
                            <td><a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_show", array("id" => $this->getAttribute($context["subcategory"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "id", array()), "html", null, true);
            echo "</a></td>
                            <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "name", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "icon", array()), "html", null, true);
            echo " <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/subcategory/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "icon", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["subcategory"], "icon", array()), "html", null, true);
            echo "\" width=\"100\" height=\"100\"></td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_show", array("id" => $this->getAttribute($context["subcategory"], "id", array()))), "html", null, true);
            echo "\">show</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_edit", array("id" => $this->getAttribute($context["subcategory"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategory'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_47fe90fb200ac2fad0624176b97fb8da42d3ca542a92228f9d4f0cd51f53ea28->leave($__internal_47fe90fb200ac2fad0624176b97fb8da42d3ca542a92228f9d4f0cd51f53ea28_prof);

        
        $__internal_01691d363870d0ed08257a957aab97755cfc106d2e5c21c1e4f4599301b61c98->leave($__internal_01691d363870d0ed08257a957aab97755cfc106d2e5c21c1e4f4599301b61c98_prof);

    }

    public function getTemplateName()
    {
        return "subcategory/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 50,  118 => 44,  112 => 41,  100 => 37,  96 => 36,  90 => 35,  87 => 34,  83 => 33,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> Sub-Categories List</h1>
            <li>
                <a href=\"{{ path('subcategory_new') }}\">Create a new subcategory</a>
            </li>
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Icon</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for subcategory in subcategories %}
                        <tr>
                            <td><a href=\"{{ path('categories_show', { 'id': subcategory.id }) }}\">{{ subcategory.id }}</a></td>
                            <td>{{ subcategory.name }}</td>
                            <td>{{ subcategory.icon }} <img src=\"{{asset('img/subcategory/')}}{{ subcategory.icon }}\" alt=\"{{ subcategory.icon }}\" width=\"100\" height=\"100\"></td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"{{ path('subcategory_show', { 'id': subcategory.id }) }}\">show</a>
                                    </li>
                                    <li>
                                        <a href=\"{{ path('subcategory_edit', { 'id': subcategory.id }) }}\">edit</a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    {% endfor %}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
{% endblock %}
", "subcategory/index.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/subcategory/index.html.twig");
    }
}
