<?php

/* sidebar.html.twig */
class __TwigTemplate_9a39683beefa1de59788cf6d8ea64edf4d2e964c35d75629c83b7e49ca2de81f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea409383de52752774842f7cc1a42369512fa2a682c9308ef05b722319a2ea1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea409383de52752774842f7cc1a42369512fa2a682c9308ef05b722319a2ea1f->enter($__internal_ea409383de52752774842f7cc1a42369512fa2a682c9308ef05b722319a2ea1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar.html.twig"));

        $__internal_d763aef6c9c4e02fa27bec0361c27f76aa793d43f376eeb9741ad68d0b257b69 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d763aef6c9c4e02fa27bec0361c27f76aa793d43f376eeb9741ad68d0b257b69->enter($__internal_d763aef6c9c4e02fa27bec0361c27f76aa793d43f376eeb9741ad68d0b257b69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar.html.twig"));

        // line 1
        echo "    <!-- Sidebar menu-->
    <div class=\"app-sidebar__overlay\" data-toggle=\"sidebar\"></div>
    <aside class=\"app-sidebar\">
      <div class=\"app-sidebar__user\"><img class=\"app-sidebar__user-avatar\" src=\"https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg\" alt=\"User Image\">
        <div>
          <p class=\"app-sidebar__user-name\">John Doe</p>
          <p class=\"app-sidebar__user-designation\">Frontend Developer</p>
        </div>
      </div>
      <ul class=\"app-menu\">
        <li><a class=\"app-menu__item\" href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\"><i class=\"app-menu__icon fa fa-dashboard\"></i><span class=\"app-menu__label\">Home</span></a></li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-th-list\"></i><span class=\"app-menu__label\">Products</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            ";
        // line 15
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 16
            echo "            <li><a class=\"treeview-item\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_new");
            echo "\"><i class=\"icon fa fa-circle-o\"></i> New product</a></li>
            ";
        }
        // line 18
        echo "          </ul>
        </li>
        ";
        // line 20
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 21
            echo "        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-edit\"></i><span class=\"app-menu__label\">Categories</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"";
            // line 23
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
            echo "\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item\" href=\"";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_new");
            echo "\"><i class=\"icon fa fa-circle-o\"></i> New category</a></li>
          </ul>
        </li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-th-list\"></i><span class=\"app-menu__label\">Users</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"";
            // line 29
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
            echo "\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item\" href=\"";
            // line 30
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_new");
            echo "\"><i class=\"icon fa fa-circle-o\"></i> New user</a></li>
          </ul>
        </li>
        ";
        }
        // line 34
        echo "      </ul>
    </aside>";
        
        $__internal_ea409383de52752774842f7cc1a42369512fa2a682c9308ef05b722319a2ea1f->leave($__internal_ea409383de52752774842f7cc1a42369512fa2a682c9308ef05b722319a2ea1f_prof);

        
        $__internal_d763aef6c9c4e02fa27bec0361c27f76aa793d43f376eeb9741ad68d0b257b69->leave($__internal_d763aef6c9c4e02fa27bec0361c27f76aa793d43f376eeb9741ad68d0b257b69_prof);

    }

    public function getTemplateName()
    {
        return "sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 34,  81 => 30,  77 => 29,  69 => 24,  65 => 23,  61 => 21,  59 => 20,  55 => 18,  49 => 16,  47 => 15,  43 => 14,  37 => 11,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    <!-- Sidebar menu-->
    <div class=\"app-sidebar__overlay\" data-toggle=\"sidebar\"></div>
    <aside class=\"app-sidebar\">
      <div class=\"app-sidebar__user\"><img class=\"app-sidebar__user-avatar\" src=\"https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg\" alt=\"User Image\">
        <div>
          <p class=\"app-sidebar__user-name\">John Doe</p>
          <p class=\"app-sidebar__user-designation\">Frontend Developer</p>
        </div>
      </div>
      <ul class=\"app-menu\">
        <li><a class=\"app-menu__item\" href=\"{{ path('homepage') }}\"><i class=\"app-menu__icon fa fa-dashboard\"></i><span class=\"app-menu__label\">Home</span></a></li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-th-list\"></i><span class=\"app-menu__label\">Products</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"{{ path('products_index') }}\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            {% if is_granted('ROLE_USER') %}
            <li><a class=\"treeview-item\" href=\"{{ path('products_new') }}\"><i class=\"icon fa fa-circle-o\"></i> New product</a></li>
            {% endif %}
          </ul>
        </li>
        {% if is_granted('ROLE_USER') %}
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-edit\"></i><span class=\"app-menu__label\">Categories</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"{{ path('categories_index') }}\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item\" href=\"{{ path('categories_new') }}\"><i class=\"icon fa fa-circle-o\"></i> New category</a></li>
          </ul>
        </li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-th-list\"></i><span class=\"app-menu__label\">Users</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"{{ path('users_index') }}\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item\" href=\"{{ path('users_new') }}\"><i class=\"icon fa fa-circle-o\"></i> New user</a></li>
          </ul>
        </li>
        {% endif %}
      </ul>
    </aside>", "sidebar.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/sidebar.html.twig");
    }
}
