<?php

/* subcategory/edit.html.twig */
class __TwigTemplate_e890e7458951e649568815ae9c2f0a8e929b0e6f5429296463c71a50ce67d849 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "subcategory/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7629e06335072305e766995246c797f277d33735b2a4a67e8b9c64b1e61b0a58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7629e06335072305e766995246c797f277d33735b2a4a67e8b9c64b1e61b0a58->enter($__internal_7629e06335072305e766995246c797f277d33735b2a4a67e8b9c64b1e61b0a58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/edit.html.twig"));

        $__internal_3382859d9539077ec5385346af6261057718674fe68a9712ce8764097c3abec3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3382859d9539077ec5385346af6261057718674fe68a9712ce8764097c3abec3->enter($__internal_3382859d9539077ec5385346af6261057718674fe68a9712ce8764097c3abec3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7629e06335072305e766995246c797f277d33735b2a4a67e8b9c64b1e61b0a58->leave($__internal_7629e06335072305e766995246c797f277d33735b2a4a67e8b9c64b1e61b0a58_prof);

        
        $__internal_3382859d9539077ec5385346af6261057718674fe68a9712ce8764097c3abec3->leave($__internal_3382859d9539077ec5385346af6261057718674fe68a9712ce8764097c3abec3_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2579376615adce1c4dbd39946319b1af9e40663c39f29cac16dacd261770cac3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2579376615adce1c4dbd39946319b1af9e40663c39f29cac16dacd261770cac3->enter($__internal_2579376615adce1c4dbd39946319b1af9e40663c39f29cac16dacd261770cac3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f0c77993bfc7d44437d7ac98f2dde9e4e018400ce78cca679278c4283a2e7d6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0c77993bfc7d44437d7ac98f2dde9e4e018400ce78cca679278c4283a2e7d6a->enter($__internal_f0c77993bfc7d44437d7ac98f2dde9e4e018400ce78cca679278c4283a2e7d6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Sub-Category edit</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

          </div>
        </div>
      </div>
      ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
    </main>
";
        
        $__internal_f0c77993bfc7d44437d7ac98f2dde9e4e018400ce78cca679278c4283a2e7d6a->leave($__internal_f0c77993bfc7d44437d7ac98f2dde9e4e018400ce78cca679278c4283a2e7d6a_prof);

        
        $__internal_2579376615adce1c4dbd39946319b1af9e40663c39f29cac16dacd261770cac3->leave($__internal_2579376615adce1c4dbd39946319b1af9e40663c39f29cac16dacd261770cac3_prof);

    }

    // line 33
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a63735372ec375e0e797d89b9f988e5afde26b2cea4aac3dfe2ff82ed0a250c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a63735372ec375e0e797d89b9f988e5afde26b2cea4aac3dfe2ff82ed0a250c9->enter($__internal_a63735372ec375e0e797d89b9f988e5afde26b2cea4aac3dfe2ff82ed0a250c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_a2e47e883eecc2aca4abd78ab1074c72aa9346819d1e20a34fa29efe1b0b244f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2e47e883eecc2aca4abd78ab1074c72aa9346819d1e20a34fa29efe1b0b244f->enter($__internal_a2e47e883eecc2aca4abd78ab1074c72aa9346819d1e20a34fa29efe1b0b244f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 34
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/newProductImg.js"), "html", null, true);
        echo "\">
";
        
        $__internal_a2e47e883eecc2aca4abd78ab1074c72aa9346819d1e20a34fa29efe1b0b244f->leave($__internal_a2e47e883eecc2aca4abd78ab1074c72aa9346819d1e20a34fa29efe1b0b244f_prof);

        
        $__internal_a63735372ec375e0e797d89b9f988e5afde26b2cea4aac3dfe2ff82ed0a250c9->leave($__internal_a63735372ec375e0e797d89b9f988e5afde26b2cea4aac3dfe2ff82ed0a250c9_prof);

    }

    public function getTemplateName()
    {
        return "subcategory/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 34,  105 => 33,  92 => 29,  87 => 27,  79 => 22,  74 => 20,  70 => 19,  56 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Sub-Category edit</h1>
          <a href=\"{{ path('subcategory_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(edit_form) }}
                {{ form_widget(edit_form) }}
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            {{ form_end(edit_form) }}

          </div>
        </div>
      </div>
      {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      {{ form_end(delete_form) }}
    </main>
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\" src=\"{{asset('js/newProductImg.js')}}\">
{% endblock %}", "subcategory/edit.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/subcategory/edit.html.twig");
    }
}
