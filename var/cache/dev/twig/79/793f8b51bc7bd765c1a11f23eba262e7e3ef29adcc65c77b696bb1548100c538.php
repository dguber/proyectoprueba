<?php

/* products/index.html.twig */
class __TwigTemplate_8a89066b15c7182658ffef0fe91757e3f2370cc8116e19ae3d12a30a7af5ad7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aca4fca46a02d3a90f6b478843282a50db71e78e65aee300a06cf6d2ba575d66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aca4fca46a02d3a90f6b478843282a50db71e78e65aee300a06cf6d2ba575d66->enter($__internal_aca4fca46a02d3a90f6b478843282a50db71e78e65aee300a06cf6d2ba575d66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/index.html.twig"));

        $__internal_8d76f34bf5a580a55e8d05a87ea801a2f7f03673e591a3f7c2f17cda6fd6e30c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d76f34bf5a580a55e8d05a87ea801a2f7f03673e591a3f7c2f17cda6fd6e30c->enter($__internal_8d76f34bf5a580a55e8d05a87ea801a2f7f03673e591a3f7c2f17cda6fd6e30c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aca4fca46a02d3a90f6b478843282a50db71e78e65aee300a06cf6d2ba575d66->leave($__internal_aca4fca46a02d3a90f6b478843282a50db71e78e65aee300a06cf6d2ba575d66_prof);

        
        $__internal_8d76f34bf5a580a55e8d05a87ea801a2f7f03673e591a3f7c2f17cda6fd6e30c->leave($__internal_8d76f34bf5a580a55e8d05a87ea801a2f7f03673e591a3f7c2f17cda6fd6e30c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_963add796d5a8026abaa65aa634f1f367ef05aab88d779f129aeb7957a53f79e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_963add796d5a8026abaa65aa634f1f367ef05aab88d779f129aeb7957a53f79e->enter($__internal_963add796d5a8026abaa65aa634f1f367ef05aab88d779f129aeb7957a53f79e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4e326fa0b9eacb30453f1acb671f7226cf2d489b4b9e799bcd7f8d1f4b2e6125 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e326fa0b9eacb30453f1acb671f7226cf2d489b4b9e799bcd7f8d1f4b2e6125->enter($__internal_4e326fa0b9eacb30453f1acb671f7226cf2d489b4b9e799bcd7f8d1f4b2e6125_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> Products List</h1>
          ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo " 
            <input type=\"submit\" value=\"Search\" class=\"btn btn-primary\"/>
          ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          ";
        // line 14
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 15
            echo "            <li>
                <a href=\"";
            // line 16
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_new");
            echo "\">New Product</a>
            </li>
          ";
        }
        // line 19
        echo "        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Subcategory</th>
                      <th>Image</th>
                      <th>Price</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 45
            echo "                        <tr>
                            <td><a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "</a></td>
                            <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["product"], "subcategory", array()), "categoryId", array()), "name", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["product"], "subcategory", array()), "name", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image", array()), "html", null, true);
            echo " <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/products/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "\" width=\"100\" height=\"100\"></td>
                            <td>\$";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_show", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
            echo "\">show</a>
                                    </li>
                                    ";
            // line 57
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
                // line 58
                echo "                                    <li>
                                        <a href=\"";
                // line 59
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_edit", array("id" => $this->getAttribute($context["product"], "id", array()))), "html", null, true);
                echo "\">edit</a>
                                    </li>
                                    ";
            }
            // line 62
            echo "                                </ul>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    

";
        
        $__internal_4e326fa0b9eacb30453f1acb671f7226cf2d489b4b9e799bcd7f8d1f4b2e6125->leave($__internal_4e326fa0b9eacb30453f1acb671f7226cf2d489b4b9e799bcd7f8d1f4b2e6125_prof);

        
        $__internal_963add796d5a8026abaa65aa634f1f367ef05aab88d779f129aeb7957a53f79e->leave($__internal_963add796d5a8026abaa65aa634f1f367ef05aab88d779f129aeb7957a53f79e_prof);

    }

    public function getTemplateName()
    {
        return "products/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 66,  165 => 62,  159 => 59,  156 => 58,  154 => 57,  149 => 55,  142 => 51,  133 => 50,  129 => 49,  125 => 48,  121 => 47,  115 => 46,  112 => 45,  108 => 44,  81 => 19,  75 => 16,  72 => 15,  70 => 14,  66 => 13,  61 => 11,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

    
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> Products List</h1>
          {{ form_start(form) }}
            {{ form_widget(form) }} 
            <input type=\"submit\" value=\"Search\" class=\"btn btn-primary\"/>
          {{ form_end(form) }}
          {% if is_granted('ROLE_USER') %}
            <li>
                <a href=\"{{ path('products_new') }}\">New Product</a>
            </li>
          {% endif %}
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Subcategory</th>
                      <th>Image</th>
                      <th>Price</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for product in products %}
                        <tr>
                            <td><a href=\"{{ path('products_show', { 'id': product.id }) }}\">{{ product.id }}</a></td>
                            <td>{{ product.name }}</td>
                            <td>{{ product.subcategory.categoryId.name }}</td>
                            <td>{{ product.subcategory.name }}</td>
                            <td>{{ product.image }} <img src=\"{{asset('img/products/')}}{{ product.image }}\" alt=\"{{ product.name }}\" width=\"100\" height=\"100\"></td>
                            <td>\${{ product.price }}</td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"{{ path('products_show', { 'id': product.id }) }}\">show</a>
                                    </li>
                                    {% if is_granted('ROLE_USER') %}
                                    <li>
                                        <a href=\"{{ path('products_edit', { 'id': product.id }) }}\">edit</a>
                                    </li>
                                    {% endif %}
                                </ul>
                            </td>
                        </tr>
                    {% endfor %}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    

{% endblock %}

", "products/index.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/products/index.html.twig");
    }
}
