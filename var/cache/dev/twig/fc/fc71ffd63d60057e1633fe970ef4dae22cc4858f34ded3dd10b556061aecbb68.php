<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_1c418f81898e61ba80215984a27c3c2395a7bb858825ed263ecd477a93074bad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e52f3f630a3ac361c92bd4d85d0f2010bb1ccb9fb9c1d3cceb94fafbf9c310e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e52f3f630a3ac361c92bd4d85d0f2010bb1ccb9fb9c1d3cceb94fafbf9c310e7->enter($__internal_e52f3f630a3ac361c92bd4d85d0f2010bb1ccb9fb9c1d3cceb94fafbf9c310e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_c5e5856d3fdad275f6c1926c21b2fb570f2a5b93e0e1d158728bf2ed93a7ca98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5e5856d3fdad275f6c1926c21b2fb570f2a5b93e0e1d158728bf2ed93a7ca98->enter($__internal_c5e5856d3fdad275f6c1926c21b2fb570f2a5b93e0e1d158728bf2ed93a7ca98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e52f3f630a3ac361c92bd4d85d0f2010bb1ccb9fb9c1d3cceb94fafbf9c310e7->leave($__internal_e52f3f630a3ac361c92bd4d85d0f2010bb1ccb9fb9c1d3cceb94fafbf9c310e7_prof);

        
        $__internal_c5e5856d3fdad275f6c1926c21b2fb570f2a5b93e0e1d158728bf2ed93a7ca98->leave($__internal_c5e5856d3fdad275f6c1926c21b2fb570f2a5b93e0e1d158728bf2ed93a7ca98_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c8587142d95f65d028e10c4170dac9edaeb98f517d1910e791cf5aa594ef5679 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8587142d95f65d028e10c4170dac9edaeb98f517d1910e791cf5aa594ef5679->enter($__internal_c8587142d95f65d028e10c4170dac9edaeb98f517d1910e791cf5aa594ef5679_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_437054c15eaee36894a3874d193b5ff20af8c6ef4f74315d45bc7cacf7cddf0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_437054c15eaee36894a3874d193b5ff20af8c6ef4f74315d45bc7cacf7cddf0d->enter($__internal_437054c15eaee36894a3874d193b5ff20af8c6ef4f74315d45bc7cacf7cddf0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_437054c15eaee36894a3874d193b5ff20af8c6ef4f74315d45bc7cacf7cddf0d->leave($__internal_437054c15eaee36894a3874d193b5ff20af8c6ef4f74315d45bc7cacf7cddf0d_prof);

        
        $__internal_c8587142d95f65d028e10c4170dac9edaeb98f517d1910e791cf5aa594ef5679->leave($__internal_c8587142d95f65d028e10c4170dac9edaeb98f517d1910e791cf5aa594ef5679_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_0a7cb22e11bedc17aa467f921b90870ef14a2838c4ae7572436d616765a027f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a7cb22e11bedc17aa467f921b90870ef14a2838c4ae7572436d616765a027f5->enter($__internal_0a7cb22e11bedc17aa467f921b90870ef14a2838c4ae7572436d616765a027f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9fcb8e502aac4fda65b3c75090aa378113433b7641e6776d35b14683c7c84982 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9fcb8e502aac4fda65b3c75090aa378113433b7641e6776d35b14683c7c84982->enter($__internal_9fcb8e502aac4fda65b3c75090aa378113433b7641e6776d35b14683c7c84982_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_9fcb8e502aac4fda65b3c75090aa378113433b7641e6776d35b14683c7c84982->leave($__internal_9fcb8e502aac4fda65b3c75090aa378113433b7641e6776d35b14683c7c84982_prof);

        
        $__internal_0a7cb22e11bedc17aa467f921b90870ef14a2838c4ae7572436d616765a027f5->leave($__internal_0a7cb22e11bedc17aa467f921b90870ef14a2838c4ae7572436d616765a027f5_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_9de8ae72eca11510e7ef98b575d518db4aa6a9359a3707c0b1d5cddfc3148691 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9de8ae72eca11510e7ef98b575d518db4aa6a9359a3707c0b1d5cddfc3148691->enter($__internal_9de8ae72eca11510e7ef98b575d518db4aa6a9359a3707c0b1d5cddfc3148691_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7d846df8ec9f02779249e4db1c676fbde2d1f8dec4c0f18a9e6e12c1c0f0cf17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d846df8ec9f02779249e4db1c676fbde2d1f8dec4c0f18a9e6e12c1c0f0cf17->enter($__internal_7d846df8ec9f02779249e4db1c676fbde2d1f8dec4c0f18a9e6e12c1c0f0cf17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_7d846df8ec9f02779249e4db1c676fbde2d1f8dec4c0f18a9e6e12c1c0f0cf17->leave($__internal_7d846df8ec9f02779249e4db1c676fbde2d1f8dec4c0f18a9e6e12c1c0f0cf17_prof);

        
        $__internal_9de8ae72eca11510e7ef98b575d518db4aa6a9359a3707c0b1d5cddfc3148691->leave($__internal_9de8ae72eca11510e7ef98b575d518db4aa6a9359a3707c0b1d5cddfc3148691_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/usuario/Documentos/www/proyectoprueba/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
