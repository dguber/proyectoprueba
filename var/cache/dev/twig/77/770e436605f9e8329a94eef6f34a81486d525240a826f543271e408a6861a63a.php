<?php

/* @WebProfiler/Profiler/header.html.twig */
class __TwigTemplate_7ad246ec54984ae86497a3ae6b9f3eb46b932d6e9e25bd72da11b2372b8a92ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e270c75546fab141ecdbf2feac91c9ef87a79474fb0a8b4d322978b90a6b0515 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e270c75546fab141ecdbf2feac91c9ef87a79474fb0a8b4d322978b90a6b0515->enter($__internal_e270c75546fab141ecdbf2feac91c9ef87a79474fb0a8b4d322978b90a6b0515_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        $__internal_3be35b3f8dfe9976c5cadf583c489b4b9344807ada5a98c9ce0669debe3e8233 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3be35b3f8dfe9976c5cadf583c489b4b9344807ada5a98c9ce0669debe3e8233->enter($__internal_3be35b3f8dfe9976c5cadf583c489b4b9344807ada5a98c9ce0669debe3e8233_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_e270c75546fab141ecdbf2feac91c9ef87a79474fb0a8b4d322978b90a6b0515->leave($__internal_e270c75546fab141ecdbf2feac91c9ef87a79474fb0a8b4d322978b90a6b0515_prof);

        
        $__internal_3be35b3f8dfe9976c5cadf583c489b4b9344807ada5a98c9ce0669debe3e8233->leave($__internal_3be35b3f8dfe9976c5cadf583c489b4b9344807ada5a98c9ce0669debe3e8233_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "@WebProfiler/Profiler/header.html.twig", "/home/usuario/Documentos/www/proyectoprueba/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
