<?php

/* navbar.html.twig */
class __TwigTemplate_4b976cf6019976001cbbdb75105a4169ec8dc0192172a5a889c2e9e459f5a6f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'usermenu' => array($this, 'block_usermenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7f293ececa5741c6a60c97bb08bb0089673aa298a9b450e3b17a903fca82845 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7f293ececa5741c6a60c97bb08bb0089673aa298a9b450e3b17a903fca82845->enter($__internal_f7f293ececa5741c6a60c97bb08bb0089673aa298a9b450e3b17a903fca82845_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        $__internal_85df9e696365603d454b2ad89d70412c9b8853439cb6794cbd9dcecde87f05e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85df9e696365603d454b2ad89d70412c9b8853439cb6794cbd9dcecde87f05e2->enter($__internal_85df9e696365603d454b2ad89d70412c9b8853439cb6794cbd9dcecde87f05e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        // line 1
        echo "    <!-- Navbar-->
    <header class=\"app-header\"><a class=\"app-header__logo\" href=\"../\">Tienda Virtual</a>
      <!-- Sidebar toggle button-->
      <a class=\"app-sidebar__toggle\" href=\"#\" data-toggle=\"sidebar\" aria-label=\"Hide Sidebar\"></a>
      <!-- Navbar Right Menu-->
      <ul class=\"app-nav\">
        <!-- User Menu-->
        ";
        // line 8
        $this->displayBlock('usermenu', $context, $blocks);
        // line 21
        echo "      </ul>
    </header>";
        
        $__internal_f7f293ececa5741c6a60c97bb08bb0089673aa298a9b450e3b17a903fca82845->leave($__internal_f7f293ececa5741c6a60c97bb08bb0089673aa298a9b450e3b17a903fca82845_prof);

        
        $__internal_85df9e696365603d454b2ad89d70412c9b8853439cb6794cbd9dcecde87f05e2->leave($__internal_85df9e696365603d454b2ad89d70412c9b8853439cb6794cbd9dcecde87f05e2_prof);

    }

    // line 8
    public function block_usermenu($context, array $blocks = array())
    {
        $__internal_416c33417fa905cc9961ff2cf22026126d1e50825242a3d669edd014d8fd4047 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_416c33417fa905cc9961ff2cf22026126d1e50825242a3d669edd014d8fd4047->enter($__internal_416c33417fa905cc9961ff2cf22026126d1e50825242a3d669edd014d8fd4047_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "usermenu"));

        $__internal_e597e899d159a22d3e8881ce5121c3670207f2dbe5260542d48488be74d9a610 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e597e899d159a22d3e8881ce5121c3670207f2dbe5260542d48488be74d9a610->enter($__internal_e597e899d159a22d3e8881ce5121c3670207f2dbe5260542d48488be74d9a610_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "usermenu"));

        // line 9
        echo "        <li class=\"dropdown\"><a class=\"app-nav__item\" href=\"#\" data-toggle=\"dropdown\" aria-label=\"Open Profile Menu\"><i class=\"fa fa-user fa-lg\"></i></a>
          <ul class=\"dropdown-menu settings-menu dropdown-menu-right\">
            ";
        // line 11
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 12
            echo "            <li><a class=\"dropdown-item\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
            echo "\"><i class=\"fa fa-cog fa-lg\"></i> Settings</a></li>
            <li><a class=\"dropdown-item\" href=\"";
            // line 13
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
            echo "\"><i class=\"fa fa-user fa-lg\"></i> Profile</a></li>
            <li><a class=\"dropdown-item\" href=\"";
            // line 14
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout");
            echo "\"><i class=\"fa fa-sign-out fa-lg\"></i> Logout</a></li>
            ";
        } else {
            // line 16
            echo "            <li><a class=\"dropdown-item\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
            echo "\"><i class=\"fa fa-sign-out fa-lg\"></i> Login</a></li>
            ";
        }
        // line 18
        echo "          </ul>
        </li>
        ";
        
        $__internal_e597e899d159a22d3e8881ce5121c3670207f2dbe5260542d48488be74d9a610->leave($__internal_e597e899d159a22d3e8881ce5121c3670207f2dbe5260542d48488be74d9a610_prof);

        
        $__internal_416c33417fa905cc9961ff2cf22026126d1e50825242a3d669edd014d8fd4047->leave($__internal_416c33417fa905cc9961ff2cf22026126d1e50825242a3d669edd014d8fd4047_prof);

    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  83 => 18,  77 => 16,  72 => 14,  68 => 13,  63 => 12,  61 => 11,  57 => 9,  48 => 8,  37 => 21,  35 => 8,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    <!-- Navbar-->
    <header class=\"app-header\"><a class=\"app-header__logo\" href=\"../\">Tienda Virtual</a>
      <!-- Sidebar toggle button-->
      <a class=\"app-sidebar__toggle\" href=\"#\" data-toggle=\"sidebar\" aria-label=\"Hide Sidebar\"></a>
      <!-- Navbar Right Menu-->
      <ul class=\"app-nav\">
        <!-- User Menu-->
        {% block usermenu %}
        <li class=\"dropdown\"><a class=\"app-nav__item\" href=\"#\" data-toggle=\"dropdown\" aria-label=\"Open Profile Menu\"><i class=\"fa fa-user fa-lg\"></i></a>
          <ul class=\"dropdown-menu settings-menu dropdown-menu-right\">
            {% if is_granted('ROLE_USER') %}
            <li><a class=\"dropdown-item\" href=\"{{ path('users_index') }}\"><i class=\"fa fa-cog fa-lg\"></i> Settings</a></li>
            <li><a class=\"dropdown-item\" href=\"{{ path('users_index') }}\"><i class=\"fa fa-user fa-lg\"></i> Profile</a></li>
            <li><a class=\"dropdown-item\" href=\"{{ path('logout') }}\"><i class=\"fa fa-sign-out fa-lg\"></i> Logout</a></li>
            {% else %}
            <li><a class=\"dropdown-item\" href=\"{{ path('login') }}\"><i class=\"fa fa-sign-out fa-lg\"></i> Login</a></li>
            {% endif %}
          </ul>
        </li>
        {% endblock %}
      </ul>
    </header>", "navbar.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/navbar.html.twig");
    }
}
