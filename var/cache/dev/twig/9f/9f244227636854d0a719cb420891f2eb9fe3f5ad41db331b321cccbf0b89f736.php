<?php

/* extra_scripts.html.twig */
class __TwigTemplate_ac36e05b7cf3288d2d63c0e64c444ba8a174dcd5f820f7b6e341f7bf7ace65b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70344469ead08459b89703aa2ffb711309bcbb024b5e07fcbbbbdd0a42c3b446 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70344469ead08459b89703aa2ffb711309bcbb024b5e07fcbbbbdd0a42c3b446->enter($__internal_70344469ead08459b89703aa2ffb711309bcbb024b5e07fcbbbbdd0a42c3b446_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "extra_scripts.html.twig"));

        $__internal_9c638652ef82951cefe53f3ae593b7cfc986e31c4e7b049b063d8acb99162c83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c638652ef82951cefe53f3ae593b7cfc986e31c4e7b049b063d8acb99162c83->enter($__internal_9c638652ef82951cefe53f3ae593b7cfc986e31c4e7b049b063d8acb99162c83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "extra_scripts.html.twig"));

        // line 1
        echo "    <!-- Essential javascripts for application to work-->
    <script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/"), "html", null, true);
        echo "jquery-3.3.1.min.js\"></script>
    <script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/"), "html", null, true);
        echo "popper.min.js\"></script>
    <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/"), "html", null, true);
        echo "bootstrap.min.js\"></script>
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/"), "html", null, true);
        echo "main.js\"></script>
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/"), "html", null, true);
        echo "app.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/plugins/"), "html", null, true);
        echo "pace.min.js\"></script>
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/plugins/"), "html", null, true);
        echo "jquery.dataTables.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/plugins/"), "html", null, true);
        echo "dataTables.bootstrap.min.js\"></script>
    <script type=\"text/javascript\">\$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type=\"text/javascript\">
      if(document.location.hostname == 'pratikborsadiya.in') {
      \t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      \t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      \tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      \t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      \tga('create', 'UA-72504830-1', 'auto');
      \tga('send', 'pageview');
      }
    </script>";
        
        $__internal_70344469ead08459b89703aa2ffb711309bcbb024b5e07fcbbbbdd0a42c3b446->leave($__internal_70344469ead08459b89703aa2ffb711309bcbb024b5e07fcbbbbdd0a42c3b446_prof);

        
        $__internal_9c638652ef82951cefe53f3ae593b7cfc986e31c4e7b049b063d8acb99162c83->leave($__internal_9c638652ef82951cefe53f3ae593b7cfc986e31c4e7b049b063d8acb99162c83_prof);

    }

    public function getTemplateName()
    {
        return "extra_scripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 12,  55 => 11,  49 => 8,  44 => 6,  40 => 5,  36 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    <!-- Essential javascripts for application to work-->
    <script src=\"{{ asset('js/') }}jquery-3.3.1.min.js\"></script>
    <script src=\"{{ asset('js/') }}popper.min.js\"></script>
    <script src=\"{{ asset('js/') }}bootstrap.min.js\"></script>
    <script src=\"{{ asset('js/') }}main.js\"></script>
    <script src=\"{{ asset('js/') }}app.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"{{ asset('js/plugins/') }}pace.min.js\"></script>
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type=\"text/javascript\" src=\"{{ asset('js/plugins/') }}jquery.dataTables.min.js\"></script>
    <script type=\"text/javascript\" src=\"{{ asset('js/plugins/') }}dataTables.bootstrap.min.js\"></script>
    <script type=\"text/javascript\">\$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type=\"text/javascript\">
      if(document.location.hostname == 'pratikborsadiya.in') {
      \t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      \t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      \tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      \t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      \tga('create', 'UA-72504830-1', 'auto');
      \tga('send', 'pageview');
      }
    </script>", "extra_scripts.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/extra_scripts.html.twig");
    }
}
