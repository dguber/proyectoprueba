<?php

/* categories/index.html.twig */
class __TwigTemplate_173d694833bd7b21287f6d60d954df1e15103c7f36301fe286ad2f0137207d82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categories/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_271067cbe201b49b5445b41e60f8918cb04c532713ce50ad131dbe597689cbe2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_271067cbe201b49b5445b41e60f8918cb04c532713ce50ad131dbe597689cbe2->enter($__internal_271067cbe201b49b5445b41e60f8918cb04c532713ce50ad131dbe597689cbe2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/index.html.twig"));

        $__internal_5924caa1b19cd57da8d480048c6059e02c4a6b1d06716771481da15bb28850b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5924caa1b19cd57da8d480048c6059e02c4a6b1d06716771481da15bb28850b4->enter($__internal_5924caa1b19cd57da8d480048c6059e02c4a6b1d06716771481da15bb28850b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_271067cbe201b49b5445b41e60f8918cb04c532713ce50ad131dbe597689cbe2->leave($__internal_271067cbe201b49b5445b41e60f8918cb04c532713ce50ad131dbe597689cbe2_prof);

        
        $__internal_5924caa1b19cd57da8d480048c6059e02c4a6b1d06716771481da15bb28850b4->leave($__internal_5924caa1b19cd57da8d480048c6059e02c4a6b1d06716771481da15bb28850b4_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c27a0638aacea5f85b67026b4e8124e595cbcbbd69d4cf5ecc7ee8714c84fa0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c27a0638aacea5f85b67026b4e8124e595cbcbbd69d4cf5ecc7ee8714c84fa0c->enter($__internal_c27a0638aacea5f85b67026b4e8124e595cbcbbd69d4cf5ecc7ee8714c84fa0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_49b40103aa4c1b458266ea90d4a377d7040021912480fdc1edc23c47e2a9d344 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49b40103aa4c1b458266ea90d4a377d7040021912480fdc1edc23c47e2a9d344->enter($__internal_49b40103aa4c1b458266ea90d4a377d7040021912480fdc1edc23c47e2a9d344_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> Categories List</h1>
            <li>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_new");
        echo "\">Create a new category</a>
            </li>
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Icon</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 34
            echo "                        <tr>
                            <td><a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_show", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "</a></td>
                            <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "icon", array()), "html", null, true);
            echo " <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/categories/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "icon", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "icon", array()), "html", null, true);
            echo "\" width=\"100\" height=\"100\"></td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_show", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\">show</a>
                                    </li>
                                    ";
            // line 43
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
                // line 44
                echo "                                    <li>
                                        <a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_edit", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
                echo "\">edit</a>
                                    </li>
                                    ";
            }
            // line 48
            echo "                                </ul>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_49b40103aa4c1b458266ea90d4a377d7040021912480fdc1edc23c47e2a9d344->leave($__internal_49b40103aa4c1b458266ea90d4a377d7040021912480fdc1edc23c47e2a9d344_prof);

        
        $__internal_c27a0638aacea5f85b67026b4e8124e595cbcbbd69d4cf5ecc7ee8714c84fa0c->leave($__internal_c27a0638aacea5f85b67026b4e8124e595cbcbbd69d4cf5ecc7ee8714c84fa0c_prof);

    }

    public function getTemplateName()
    {
        return "categories/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 52,  128 => 48,  122 => 45,  119 => 44,  117 => 43,  112 => 41,  100 => 37,  96 => 36,  90 => 35,  87 => 34,  83 => 33,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-th-list\"></i> Categories List</h1>
            <li>
                <a href=\"{{ path('categories_new') }}\">Create a new category</a>
            </li>
        </div>
        <ul class=\"app-breadcrumb breadcrumb side\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Tables</li>
          <li class=\"breadcrumb-item active\"><a href=\"#\">Data Table</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"tile\">
            <div class=\"tile-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-hover table-bordered\" id=\"sampleTable\">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Icon</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for category in categories %}
                        <tr>
                            <td><a href=\"{{ path('categories_show', { 'id': category.id }) }}\">{{ category.id }}</a></td>
                            <td>{{ category.name }}</td>
                            <td>{{ category.icon }} <img src=\"{{asset('img/categories/')}}{{ category.icon }}\" alt=\"{{ category.icon }}\" width=\"100\" height=\"100\"></td>
                            <td>
                                <ul>
                                    <li>
                                        <a href=\"{{ path('categories_show', { 'id': category.id }) }}\">show</a>
                                    </li>
                                    {% if is_granted('ROLE_USER') %}
                                    <li>
                                        <a href=\"{{ path('categories_edit', { 'id': category.id }) }}\">edit</a>
                                    </li>
                                    {% endif %}
                                </ul>
                            </td>
                        </tr>
                    {% endfor %}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
{% endblock %}
", "categories/index.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/categories/index.html.twig");
    }
}
