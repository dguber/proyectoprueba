<?php

/* registration/register.html.twig */
class __TwigTemplate_94038306b87314c91b5c2d6e501a67d739aff4526713a4fa7edef5ae1425e83b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bfbd912ec5b73a9f8f38a28fbe8a6fb8ac0d43aa9fd5c8850357e35a29c7e373 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bfbd912ec5b73a9f8f38a28fbe8a6fb8ac0d43aa9fd5c8850357e35a29c7e373->enter($__internal_bfbd912ec5b73a9f8f38a28fbe8a6fb8ac0d43aa9fd5c8850357e35a29c7e373_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "registration/register.html.twig"));

        $__internal_dfe19f48b270f717029eab5b0a826a135c610e3037ca414f43c94150172cef7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfe19f48b270f717029eab5b0a826a135c610e3037ca414f43c94150172cef7e->enter($__internal_dfe19f48b270f717029eab5b0a826a135c610e3037ca414f43c94150172cef7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "registration/register.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- Main CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">
    <!-- Font-icon css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <title>Register - Vali Admin</title>
  </head>
  ";
        // line 13
        $this->displayBlock('body', $context, $blocks);
        // line 65
        echo "</html>";
        
        $__internal_bfbd912ec5b73a9f8f38a28fbe8a6fb8ac0d43aa9fd5c8850357e35a29c7e373->leave($__internal_bfbd912ec5b73a9f8f38a28fbe8a6fb8ac0d43aa9fd5c8850357e35a29c7e373_prof);

        
        $__internal_dfe19f48b270f717029eab5b0a826a135c610e3037ca414f43c94150172cef7e->leave($__internal_dfe19f48b270f717029eab5b0a826a135c610e3037ca414f43c94150172cef7e_prof);

    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        $__internal_755862c0dbd964981ae87a2b48fe5edc2716742d7eecc6e54b927021304aaeb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_755862c0dbd964981ae87a2b48fe5edc2716742d7eecc6e54b927021304aaeb2->enter($__internal_755862c0dbd964981ae87a2b48fe5edc2716742d7eecc6e54b927021304aaeb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_57c9da43f44a943957352b9d524212a8427681b97ae2a04f32e4de5ef047d3f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57c9da43f44a943957352b9d524212a8427681b97ae2a04f32e4de5ef047d3f7->enter($__internal_57c9da43f44a943957352b9d524212a8427681b97ae2a04f32e4de5ef047d3f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 14
        echo "  <body>
    <section class=\"material-half-bg\">
      <div class=\"cover\"></div>
    </section>
    <section class=\"login-content\">
      <div class=\"logo\">
        <h1>Vali</h1>
      </div>
      ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
      <div class=\"login-box\">
        <div class=\"login-form\">
          <h3 class=\"login-head\"><i class=\"fa fa-lg fa-fw fa-user\"></i>Register</h3>
          <div class=\"form-group\">
            ";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'label', array("attr" => array("class" => "control-label"), "label" => "USERNAME"));
        echo "
            ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
          </div>
          <div class=\"form-group\">
            ";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'label', array("attr" => array("class" => "control-label"), "label" => "EMAIL"));
        echo "
            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
          </div>
          <div class=\"form-group\">
            ";
        // line 35
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'label', array("attr" => array("class" => "control-label"), "label" => "FIRST PASSWORD"));
        echo "
            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
            ";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'label', array("attr" => array("class" => "control-label"), "label" => "REPEAT PASSWORD"));
        echo "
    \t\t    ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'row', array("attr" => array("class" => "form-control")));
        echo "
            <input type=\"hidden\" name=\"go_to\" value=\"/products\" />
          </div>
          
          <div class=\"form-group btn-container\">
            <button class=\"btn btn-primary btn-block\" type=\"submit\"><i class=\"fa fa-sign-in fa-lg fa-fw\"></i>Register</button>
          </div>
        </div>
      </div>
      ";
        // line 47
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </section>
    <!-- Essential javascripts for application to work-->
    <script src=\"js/jquery-3.3.1.min.js\"></script>
    <script src=\"js/popper.min.js\"></script>
    <script src=\"js/bootstrap.min.js\"></script>
    <script src=\"js/main.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"js/plugins/pace.min.js\"></script>
    <script type=\"text/javascript\">
      // Login Page Flipbox control
      \$('.login-content [data-toggle=\"flip\"]').click(function() {
        \$('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
  ";
        
        $__internal_57c9da43f44a943957352b9d524212a8427681b97ae2a04f32e4de5ef047d3f7->leave($__internal_57c9da43f44a943957352b9d524212a8427681b97ae2a04f32e4de5ef047d3f7_prof);

        
        $__internal_755862c0dbd964981ae87a2b48fe5edc2716742d7eecc6e54b927021304aaeb2->leave($__internal_755862c0dbd964981ae87a2b48fe5edc2716742d7eecc6e54b927021304aaeb2_prof);

    }

    public function getTemplateName()
    {
        return "registration/register.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  123 => 47,  111 => 38,  107 => 37,  103 => 36,  99 => 35,  93 => 32,  89 => 31,  83 => 28,  79 => 27,  71 => 22,  61 => 14,  52 => 13,  42 => 65,  40 => 13,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- Main CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">
    <!-- Font-icon css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <title>Register - Vali Admin</title>
  </head>
  {% block body %}
  <body>
    <section class=\"material-half-bg\">
      <div class=\"cover\"></div>
    </section>
    <section class=\"login-content\">
      <div class=\"logo\">
        <h1>Vali</h1>
      </div>
      {{ form_start(form) }}
      <div class=\"login-box\">
        <div class=\"login-form\">
          <h3 class=\"login-head\"><i class=\"fa fa-lg fa-fw fa-user\"></i>Register</h3>
          <div class=\"form-group\">
            {{ form_label(form.username, 'USERNAME', {'attr': {'class': 'control-label'}} ) }}
            {{ form_row(form.username, {'attr': {'class': 'form-control'}} ) }}
          </div>
          <div class=\"form-group\">
            {{ form_label(form.email, 'EMAIL', {'attr': {'class': 'control-label'}} ) }}
            {{ form_row(form.email, {'attr': {'class': 'form-control'}} ) }}
          </div>
          <div class=\"form-group\">
            {{ form_label(form.plainPassword.first, 'FIRST PASSWORD', {'attr': {'class': 'control-label'}} ) }}
            {{ form_row(form.plainPassword.first, {'attr': {'class': 'form-control'}} ) }}
            {{ form_label(form.plainPassword.second, 'REPEAT PASSWORD', {'attr': {'class': 'control-label'}} ) }}
    \t\t    {{ form_row(form.plainPassword.second, {'attr': {'class': 'form-control'}} ) }}
            <input type=\"hidden\" name=\"go_to\" value=\"/products\" />
          </div>
          
          <div class=\"form-group btn-container\">
            <button class=\"btn btn-primary btn-block\" type=\"submit\"><i class=\"fa fa-sign-in fa-lg fa-fw\"></i>Register</button>
          </div>
        </div>
      </div>
      {{ form_end(form) }}
    </section>
    <!-- Essential javascripts for application to work-->
    <script src=\"js/jquery-3.3.1.min.js\"></script>
    <script src=\"js/popper.min.js\"></script>
    <script src=\"js/bootstrap.min.js\"></script>
    <script src=\"js/main.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"js/plugins/pace.min.js\"></script>
    <script type=\"text/javascript\">
      // Login Page Flipbox control
      \$('.login-content [data-toggle=\"flip\"]').click(function() {
        \$('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
  {% endblock %}
</html>", "registration/register.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/registration/register.html.twig");
    }
}
