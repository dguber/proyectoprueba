<?php

/* categories/new.html.twig */
class __TwigTemplate_31a344543d951e7f9513f864699f8197ba2064563ffc0d9f6186671b92641009 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categories/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a550df260fd24aecb35a75710a0b6773957081bfc1fbb3409382e131719d13b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a550df260fd24aecb35a75710a0b6773957081bfc1fbb3409382e131719d13b0->enter($__internal_a550df260fd24aecb35a75710a0b6773957081bfc1fbb3409382e131719d13b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/new.html.twig"));

        $__internal_593b2f08c5709df65f6cfe1c048f6aed4b5bade844c4cf1bc78e810e845f48c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_593b2f08c5709df65f6cfe1c048f6aed4b5bade844c4cf1bc78e810e845f48c1->enter($__internal_593b2f08c5709df65f6cfe1c048f6aed4b5bade844c4cf1bc78e810e845f48c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a550df260fd24aecb35a75710a0b6773957081bfc1fbb3409382e131719d13b0->leave($__internal_a550df260fd24aecb35a75710a0b6773957081bfc1fbb3409382e131719d13b0_prof);

        
        $__internal_593b2f08c5709df65f6cfe1c048f6aed4b5bade844c4cf1bc78e810e845f48c1->leave($__internal_593b2f08c5709df65f6cfe1c048f6aed4b5bade844c4cf1bc78e810e845f48c1_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6646cec714af814c1a96723b8f2eb9079e239c96f631f411d5fa321480b17416 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6646cec714af814c1a96723b8f2eb9079e239c96f631f411d5fa321480b17416->enter($__internal_6646cec714af814c1a96723b8f2eb9079e239c96f631f411d5fa321480b17416_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_eb3fc2078297077fe884e166d61197719a1c0e49c4f5ff1580c979c2f04adc00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb3fc2078297077fe884e166d61197719a1c0e49c4f5ff1580c979c2f04adc00->enter($__internal_eb3fc2078297077fe884e166d61197719a1c0e49c4f5ff1580c979c2f04adc00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Category creation</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_eb3fc2078297077fe884e166d61197719a1c0e49c4f5ff1580c979c2f04adc00->leave($__internal_eb3fc2078297077fe884e166d61197719a1c0e49c4f5ff1580c979c2f04adc00_prof);

        
        $__internal_6646cec714af814c1a96723b8f2eb9079e239c96f631f411d5fa321480b17416->leave($__internal_6646cec714af814c1a96723b8f2eb9079e239c96f631f411d5fa321480b17416_prof);

    }

    // line 29
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1c3548a7d3f5be0a2b22af98b7616a97fcfd2b7ab17c352607626a61c9810ffb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c3548a7d3f5be0a2b22af98b7616a97fcfd2b7ab17c352607626a61c9810ffb->enter($__internal_1c3548a7d3f5be0a2b22af98b7616a97fcfd2b7ab17c352607626a61c9810ffb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d49a4b52fc46656757039ffee021f876eb810c7fb275a23c1397695694e60c60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d49a4b52fc46656757039ffee021f876eb810c7fb275a23c1397695694e60c60->enter($__internal_d49a4b52fc46656757039ffee021f876eb810c7fb275a23c1397695694e60c60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 30
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/newProductImg.js"), "html", null, true);
        echo "\">
";
        
        $__internal_d49a4b52fc46656757039ffee021f876eb810c7fb275a23c1397695694e60c60->leave($__internal_d49a4b52fc46656757039ffee021f876eb810c7fb275a23c1397695694e60c60_prof);

        
        $__internal_1c3548a7d3f5be0a2b22af98b7616a97fcfd2b7ab17c352607626a61c9810ffb->leave($__internal_1c3548a7d3f5be0a2b22af98b7616a97fcfd2b7ab17c352607626a61c9810ffb_prof);

    }

    public function getTemplateName()
    {
        return "categories/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  95 => 29,  79 => 22,  74 => 20,  70 => 19,  56 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Category creation</h1>
          <a href=\"{{ path('categories_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(form) }}
                {{ form_widget(form) }}
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\"/>
            {{ form_end(form) }}
          </div>
        </div>
      </div>
    </main>
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\" src=\"{{asset('js/newProductImg.js')}}\">
{% endblock %}

", "categories/new.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/categories/new.html.twig");
    }
}
