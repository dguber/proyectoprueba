<?php

/* subcategory/show.html.twig */
class __TwigTemplate_a1cd76ade44da51ca2403e6aac3a26ed584b258863c4b40ea15c8e1235ad1209 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "subcategory/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4834681a53d37146004d9b642446628f9ba5ddc3461931dbc095d7ad67c81470 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4834681a53d37146004d9b642446628f9ba5ddc3461931dbc095d7ad67c81470->enter($__internal_4834681a53d37146004d9b642446628f9ba5ddc3461931dbc095d7ad67c81470_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/show.html.twig"));

        $__internal_3b5c5080173354f7d26853e9683ac46e08e545beffa11d40754ffbbf96ffbfe6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b5c5080173354f7d26853e9683ac46e08e545beffa11d40754ffbbf96ffbfe6->enter($__internal_3b5c5080173354f7d26853e9683ac46e08e545beffa11d40754ffbbf96ffbfe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4834681a53d37146004d9b642446628f9ba5ddc3461931dbc095d7ad67c81470->leave($__internal_4834681a53d37146004d9b642446628f9ba5ddc3461931dbc095d7ad67c81470_prof);

        
        $__internal_3b5c5080173354f7d26853e9683ac46e08e545beffa11d40754ffbbf96ffbfe6->leave($__internal_3b5c5080173354f7d26853e9683ac46e08e545beffa11d40754ffbbf96ffbfe6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_89f8bf6530c4087a5a85717c7375a03a36151aa973bbbadd300ec070c0100c2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89f8bf6530c4087a5a85717c7375a03a36151aa973bbbadd300ec070c0100c2f->enter($__internal_89f8bf6530c4087a5a85717c7375a03a36151aa973bbbadd300ec070c0100c2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_953e61d4941bb14407536caae53467578e71ba2bcf3b94fc66ad098e7d54322c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_953e61d4941bb14407536caae53467578e71ba2bcf3b94fc66ad098e7d54322c->enter($__internal_953e61d4941bb14407536caae53467578e71ba2bcf3b94fc66ad098e7d54322c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> Sub-Category</h1>
            <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_index");
        echo "\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "id", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "name", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Parent Category: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "categoryid", array()), "name", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    Image: ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "icon", array()), "html", null, true);
        echo "
                    <img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/subcategory/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "icon", array()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "icon", array()), "html", null, true);
        echo "\" width=\"100\" height=\"100\">
                </li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        <li>
                            <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_edit", array("id" => $this->getAttribute(($context["subcategory"] ?? $this->getContext($context, "subcategory")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
                        </li>
                        <li>
                            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
                        </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
";
        
        $__internal_953e61d4941bb14407536caae53467578e71ba2bcf3b94fc66ad098e7d54322c->leave($__internal_953e61d4941bb14407536caae53467578e71ba2bcf3b94fc66ad098e7d54322c_prof);

        
        $__internal_89f8bf6530c4087a5a85717c7375a03a36151aa973bbbadd300ec070c0100c2f->leave($__internal_89f8bf6530c4087a5a85717c7375a03a36151aa973bbbadd300ec070c0100c2f_prof);

    }

    public function getTemplateName()
    {
        return "subcategory/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 35,  104 => 33,  98 => 30,  87 => 25,  83 => 24,  78 => 22,  74 => 21,  70 => 20,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> Sub-Category</h1>
            <a href=\"{{ path('subcategory_index') }}\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: {{ subcategory.id }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: {{ subcategory.name }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Parent Category: {{ subcategory.categoryid.name }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    Image: {{ subcategory.icon }}
                    <img src=\"{{asset('img/subcategory/')}}{{ subcategory.icon }}\" alt=\"{{ subcategory.icon }}\" width=\"100\" height=\"100\">
                </li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        <li>
                            <a href=\"{{ path('subcategory_edit', { 'id': subcategory.id }) }}\">Edit</a>
                        </li>
                        <li>
                            {{ form_start(delete_form) }}
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            {{ form_end(delete_form) }}
                        </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
{% endblock %}
", "subcategory/show.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/subcategory/show.html.twig");
    }
}
