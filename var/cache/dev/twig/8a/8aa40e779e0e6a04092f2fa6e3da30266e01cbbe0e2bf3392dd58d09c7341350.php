<?php

/* products/edit.html.twig */
class __TwigTemplate_602af1c3399783d7b3ddbf50d3b577f0553a8755df9c5eec12b9b677bb7d4101 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_859ab087bc46e34c77aeade026b688f74892904d548eafd7b6c6cb979f6155d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_859ab087bc46e34c77aeade026b688f74892904d548eafd7b6c6cb979f6155d4->enter($__internal_859ab087bc46e34c77aeade026b688f74892904d548eafd7b6c6cb979f6155d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/edit.html.twig"));

        $__internal_26fe9e32a2735184e20a95f5d7acdad4c6812f9b037749f58d62e707bb4cbf08 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26fe9e32a2735184e20a95f5d7acdad4c6812f9b037749f58d62e707bb4cbf08->enter($__internal_26fe9e32a2735184e20a95f5d7acdad4c6812f9b037749f58d62e707bb4cbf08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_859ab087bc46e34c77aeade026b688f74892904d548eafd7b6c6cb979f6155d4->leave($__internal_859ab087bc46e34c77aeade026b688f74892904d548eafd7b6c6cb979f6155d4_prof);

        
        $__internal_26fe9e32a2735184e20a95f5d7acdad4c6812f9b037749f58d62e707bb4cbf08->leave($__internal_26fe9e32a2735184e20a95f5d7acdad4c6812f9b037749f58d62e707bb4cbf08_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b6bf0be8c5de1e34ff2868d3f558a7dd781cdfe43a8a18d89c45ef6a76d22abd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6bf0be8c5de1e34ff2868d3f558a7dd781cdfe43a8a18d89c45ef6a76d22abd->enter($__internal_b6bf0be8c5de1e34ff2868d3f558a7dd781cdfe43a8a18d89c45ef6a76d22abd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_dc28de797c310e46b394d02ba6c34bc0d6b7cb71fcbd21fa6a9e80b176c8a8f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc28de797c310e46b394d02ba6c34bc0d6b7cb71fcbd21fa6a9e80b176c8a8f7->enter($__internal_dc28de797c310e46b394d02ba6c34bc0d6b7cb71fcbd21fa6a9e80b176c8a8f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Product edit</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

          </div>
        </div>
      </div>
      ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
    </main>
";
        
        $__internal_dc28de797c310e46b394d02ba6c34bc0d6b7cb71fcbd21fa6a9e80b176c8a8f7->leave($__internal_dc28de797c310e46b394d02ba6c34bc0d6b7cb71fcbd21fa6a9e80b176c8a8f7_prof);

        
        $__internal_b6bf0be8c5de1e34ff2868d3f558a7dd781cdfe43a8a18d89c45ef6a76d22abd->leave($__internal_b6bf0be8c5de1e34ff2868d3f558a7dd781cdfe43a8a18d89c45ef6a76d22abd_prof);

    }

    // line 33
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e3b498a95a60b88e69ac885c5c18577169d292f4e593caf87a1bbdcbf9fe3865 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3b498a95a60b88e69ac885c5c18577169d292f4e593caf87a1bbdcbf9fe3865->enter($__internal_e3b498a95a60b88e69ac885c5c18577169d292f4e593caf87a1bbdcbf9fe3865_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_823e4d60e31ca672a21bdbc88f789297730ef0f4f173c469398193634c57e2f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_823e4d60e31ca672a21bdbc88f789297730ef0f4f173c469398193634c57e2f4->enter($__internal_823e4d60e31ca672a21bdbc88f789297730ef0f4f173c469398193634c57e2f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 34
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/newProductImg.js"), "html", null, true);
        echo "\">
";
        
        $__internal_823e4d60e31ca672a21bdbc88f789297730ef0f4f173c469398193634c57e2f4->leave($__internal_823e4d60e31ca672a21bdbc88f789297730ef0f4f173c469398193634c57e2f4_prof);

        
        $__internal_e3b498a95a60b88e69ac885c5c18577169d292f4e593caf87a1bbdcbf9fe3865->leave($__internal_e3b498a95a60b88e69ac885c5c18577169d292f4e593caf87a1bbdcbf9fe3865_prof);

    }

    public function getTemplateName()
    {
        return "products/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 34,  105 => 33,  92 => 29,  87 => 27,  79 => 22,  74 => 20,  70 => 19,  56 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Product edit</h1>
          <a href=\"{{ path('products_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(edit_form) }}
                {{ form_widget(edit_form) }}
                <input type=\"submit\" value=\"Edit\" class=\"btn btn-primary\"/>
            {{ form_end(edit_form) }}

          </div>
        </div>
      </div>
      {{ form_start(delete_form) }}
        <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
      {{ form_end(delete_form) }}
    </main>
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\" src=\"{{asset('js/newProductImg.js')}}\">
{% endblock %}
", "products/edit.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/products/edit.html.twig");
    }
}
