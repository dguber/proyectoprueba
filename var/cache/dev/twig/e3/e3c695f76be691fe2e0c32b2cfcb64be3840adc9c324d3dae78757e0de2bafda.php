<?php

/* security/login.html.twig */
class __TwigTemplate_51e44f4900f8ca50d1238959dc7306956d6c8151c1974c7264280dde5b405fba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6c5e1a968ac866f5e13bb7239d5ec52d1f288b0d21f2ea5d8d807fbeaa69e37 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6c5e1a968ac866f5e13bb7239d5ec52d1f288b0d21f2ea5d8d807fbeaa69e37->enter($__internal_e6c5e1a968ac866f5e13bb7239d5ec52d1f288b0d21f2ea5d8d807fbeaa69e37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_6651a98105b009727861a68c01f91fd1ae195be7369d083fe59e106570a41c65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6651a98105b009727861a68c01f91fd1ae195be7369d083fe59e106570a41c65->enter($__internal_6651a98105b009727861a68c01f91fd1ae195be7369d083fe59e106570a41c65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- Main CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">
    <!-- Font-icon css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <title>Login - Vali Admin</title>
  </head>
  ";
        // line 13
        $this->displayBlock('body', $context, $blocks);
        // line 61
        echo "</html>";
        
        $__internal_e6c5e1a968ac866f5e13bb7239d5ec52d1f288b0d21f2ea5d8d807fbeaa69e37->leave($__internal_e6c5e1a968ac866f5e13bb7239d5ec52d1f288b0d21f2ea5d8d807fbeaa69e37_prof);

        
        $__internal_6651a98105b009727861a68c01f91fd1ae195be7369d083fe59e106570a41c65->leave($__internal_6651a98105b009727861a68c01f91fd1ae195be7369d083fe59e106570a41c65_prof);

    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        $__internal_b01aa1dcc0591eef86f8eb03a464e4941e34fb46655a775ce3eca03e6d73d760 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b01aa1dcc0591eef86f8eb03a464e4941e34fb46655a775ce3eca03e6d73d760->enter($__internal_b01aa1dcc0591eef86f8eb03a464e4941e34fb46655a775ce3eca03e6d73d760_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6645ee2166551513627425fcf5dc02ce55c2f57340a6aff249a054edeba9ec59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6645ee2166551513627425fcf5dc02ce55c2f57340a6aff249a054edeba9ec59->enter($__internal_6645ee2166551513627425fcf5dc02ce55c2f57340a6aff249a054edeba9ec59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 14
        echo "  <body>
    <section class=\"material-half-bg\">
      <div class=\"cover\"></div>
    </section>
    <section class=\"login-content\">
      <div class=\"logo\">
        <h1>Vali</h1>
      </div>
      <div class=\"login-box\">
        <form class=\"login-form\" action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
          <h3 class=\"login-head\"><i class=\"fa fa-lg fa-fw fa-user\"></i>Sign in</h3>
          <div class=\"form-group\">
            <label class=\"control-label\">USERNAME</label>
            <input class=\"form-control\" type=\"text\" placeholder=\"Username\" id=\"username\" name=\"_username\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" autofocus>
          </div>
          <div class=\"form-group\">
            <label class=\"control-label\">PASSWORD</label>
            <input class=\"form-control\" type=\"password\" placeholder=\"Password\" id=\"password\" name=\"_password\">
            <input type=\"hidden\" name=\"go_to\" value=\"/products\" />
          </div>
          <div class=\"form-group\">
            ";
        // line 35
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 36
            echo "                <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
            ";
        }
        // line 38
        echo "          </div>
          <div class=\"form-group btn-container\">
            <button class=\"btn btn-primary btn-block\" type=\"submit\"><i class=\"fa fa-sign-in fa-lg fa-fw\"></i>Sign in</button>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src=\"js/jquery-3.3.1.min.js\"></script>
    <script src=\"js/popper.min.js\"></script>
    <script src=\"js/bootstrap.min.js\"></script>
    <script src=\"js/main.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"js/plugins/pace.min.js\"></script>
    <script type=\"text/javascript\">
      // Login Page Flipbox control
      \$('.login-content [data-toggle=\"flip\"]').click(function() {
        \$('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
  ";
        
        $__internal_6645ee2166551513627425fcf5dc02ce55c2f57340a6aff249a054edeba9ec59->leave($__internal_6645ee2166551513627425fcf5dc02ce55c2f57340a6aff249a054edeba9ec59_prof);

        
        $__internal_b01aa1dcc0591eef86f8eb03a464e4941e34fb46655a775ce3eca03e6d73d760->leave($__internal_b01aa1dcc0591eef86f8eb03a464e4941e34fb46655a775ce3eca03e6d73d760_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  98 => 38,  92 => 36,  90 => 35,  79 => 27,  72 => 23,  61 => 14,  52 => 13,  42 => 61,  40 => 13,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- Main CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">
    <!-- Font-icon css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <title>Login - Vali Admin</title>
  </head>
  {% block body %}
  <body>
    <section class=\"material-half-bg\">
      <div class=\"cover\"></div>
    </section>
    <section class=\"login-content\">
      <div class=\"logo\">
        <h1>Vali</h1>
      </div>
      <div class=\"login-box\">
        <form class=\"login-form\" action=\"{{ path('login') }}\" method=\"post\">
          <h3 class=\"login-head\"><i class=\"fa fa-lg fa-fw fa-user\"></i>Sign in</h3>
          <div class=\"form-group\">
            <label class=\"control-label\">USERNAME</label>
            <input class=\"form-control\" type=\"text\" placeholder=\"Username\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" autofocus>
          </div>
          <div class=\"form-group\">
            <label class=\"control-label\">PASSWORD</label>
            <input class=\"form-control\" type=\"password\" placeholder=\"Password\" id=\"password\" name=\"_password\">
            <input type=\"hidden\" name=\"go_to\" value=\"/products\" />
          </div>
          <div class=\"form-group\">
            {% if error %}
                <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
            {% endif %}
          </div>
          <div class=\"form-group btn-container\">
            <button class=\"btn btn-primary btn-block\" type=\"submit\"><i class=\"fa fa-sign-in fa-lg fa-fw\"></i>Sign in</button>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src=\"js/jquery-3.3.1.min.js\"></script>
    <script src=\"js/popper.min.js\"></script>
    <script src=\"js/bootstrap.min.js\"></script>
    <script src=\"js/main.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"js/plugins/pace.min.js\"></script>
    <script type=\"text/javascript\">
      // Login Page Flipbox control
      \$('.login-content [data-toggle=\"flip\"]').click(function() {
        \$('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
  {% endblock %}
</html>", "security/login.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/security/login.html.twig");
    }
}
