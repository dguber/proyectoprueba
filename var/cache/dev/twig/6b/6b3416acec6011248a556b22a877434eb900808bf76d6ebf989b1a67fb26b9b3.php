<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_21fedabfb2752bd9c2c41fbf26cd85b6022da5a9885d379c074c6c2dedf6a531 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bff4ae9f86c80b6446469c26132728cec8ee156b4206ece3f79d72e769107b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bff4ae9f86c80b6446469c26132728cec8ee156b4206ece3f79d72e769107b0->enter($__internal_2bff4ae9f86c80b6446469c26132728cec8ee156b4206ece3f79d72e769107b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_aff01460d30c5c9cbf33615a7716804310e5e1fcb8ff5451cc248f93d01852fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aff01460d30c5c9cbf33615a7716804310e5e1fcb8ff5451cc248f93d01852fb->enter($__internal_aff01460d30c5c9cbf33615a7716804310e5e1fcb8ff5451cc248f93d01852fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2bff4ae9f86c80b6446469c26132728cec8ee156b4206ece3f79d72e769107b0->leave($__internal_2bff4ae9f86c80b6446469c26132728cec8ee156b4206ece3f79d72e769107b0_prof);

        
        $__internal_aff01460d30c5c9cbf33615a7716804310e5e1fcb8ff5451cc248f93d01852fb->leave($__internal_aff01460d30c5c9cbf33615a7716804310e5e1fcb8ff5451cc248f93d01852fb_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6936ce6b6d34a856bf174d208ac17a26e956233600681292325408026ca68f22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6936ce6b6d34a856bf174d208ac17a26e956233600681292325408026ca68f22->enter($__internal_6936ce6b6d34a856bf174d208ac17a26e956233600681292325408026ca68f22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_372692e974cd5304620c099e9158c23042c445b9d925672a49b470d0edeffb26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_372692e974cd5304620c099e9158c23042c445b9d925672a49b470d0edeffb26->enter($__internal_372692e974cd5304620c099e9158c23042c445b9d925672a49b470d0edeffb26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_372692e974cd5304620c099e9158c23042c445b9d925672a49b470d0edeffb26->leave($__internal_372692e974cd5304620c099e9158c23042c445b9d925672a49b470d0edeffb26_prof);

        
        $__internal_6936ce6b6d34a856bf174d208ac17a26e956233600681292325408026ca68f22->leave($__internal_6936ce6b6d34a856bf174d208ac17a26e956233600681292325408026ca68f22_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_71f89df8b0c4b2c1307e638a06b2847cfbb486d7336a122784c29e68c4f67734 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71f89df8b0c4b2c1307e638a06b2847cfbb486d7336a122784c29e68c4f67734->enter($__internal_71f89df8b0c4b2c1307e638a06b2847cfbb486d7336a122784c29e68c4f67734_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_b975de6ba8a846c4b22d57a8200a6bc7bb07c91a26f251193d97bce8d5fea94e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b975de6ba8a846c4b22d57a8200a6bc7bb07c91a26f251193d97bce8d5fea94e->enter($__internal_b975de6ba8a846c4b22d57a8200a6bc7bb07c91a26f251193d97bce8d5fea94e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_b975de6ba8a846c4b22d57a8200a6bc7bb07c91a26f251193d97bce8d5fea94e->leave($__internal_b975de6ba8a846c4b22d57a8200a6bc7bb07c91a26f251193d97bce8d5fea94e_prof);

        
        $__internal_71f89df8b0c4b2c1307e638a06b2847cfbb486d7336a122784c29e68c4f67734->leave($__internal_71f89df8b0c4b2c1307e638a06b2847cfbb486d7336a122784c29e68c4f67734_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_dbb12fdf0cd5daa3e5d878e36a487bbb3c0a3c8a1f3041e37ed1bb157cfa00f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbb12fdf0cd5daa3e5d878e36a487bbb3c0a3c8a1f3041e37ed1bb157cfa00f2->enter($__internal_dbb12fdf0cd5daa3e5d878e36a487bbb3c0a3c8a1f3041e37ed1bb157cfa00f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b70406f031d9d6ef4983d651adaabf6cf2990e56f33bf13d199e084b2ab48eb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b70406f031d9d6ef4983d651adaabf6cf2990e56f33bf13d199e084b2ab48eb6->enter($__internal_b70406f031d9d6ef4983d651adaabf6cf2990e56f33bf13d199e084b2ab48eb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_b70406f031d9d6ef4983d651adaabf6cf2990e56f33bf13d199e084b2ab48eb6->leave($__internal_b70406f031d9d6ef4983d651adaabf6cf2990e56f33bf13d199e084b2ab48eb6_prof);

        
        $__internal_dbb12fdf0cd5daa3e5d878e36a487bbb3c0a3c8a1f3041e37ed1bb157cfa00f2->leave($__internal_dbb12fdf0cd5daa3e5d878e36a487bbb3c0a3c8a1f3041e37ed1bb157cfa00f2_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/usuario/Documentos/www/proyectoprueba/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
