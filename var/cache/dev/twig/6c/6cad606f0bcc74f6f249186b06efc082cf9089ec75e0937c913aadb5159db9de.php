<?php

/* products/new.html.twig */
class __TwigTemplate_a890deefa6efd04ffa9d7e8040fe119361615f69b916934f7ce647fb4ac0beb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_152d02eccdb80752419a4a97ae66a8cf14e6e6e0ee291e0551bb00fe7ce05705 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_152d02eccdb80752419a4a97ae66a8cf14e6e6e0ee291e0551bb00fe7ce05705->enter($__internal_152d02eccdb80752419a4a97ae66a8cf14e6e6e0ee291e0551bb00fe7ce05705_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/new.html.twig"));

        $__internal_9da962046b4a959c0e47b90a36f583e5fe20a4f2a712b426dd09a18b11b20d22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9da962046b4a959c0e47b90a36f583e5fe20a4f2a712b426dd09a18b11b20d22->enter($__internal_9da962046b4a959c0e47b90a36f583e5fe20a4f2a712b426dd09a18b11b20d22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_152d02eccdb80752419a4a97ae66a8cf14e6e6e0ee291e0551bb00fe7ce05705->leave($__internal_152d02eccdb80752419a4a97ae66a8cf14e6e6e0ee291e0551bb00fe7ce05705_prof);

        
        $__internal_9da962046b4a959c0e47b90a36f583e5fe20a4f2a712b426dd09a18b11b20d22->leave($__internal_9da962046b4a959c0e47b90a36f583e5fe20a4f2a712b426dd09a18b11b20d22_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b1c7242b8bf916001495d392ab7bac4a160f3eae40b77a5ec355f7e1cea4b47f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1c7242b8bf916001495d392ab7bac4a160f3eae40b77a5ec355f7e1cea4b47f->enter($__internal_b1c7242b8bf916001495d392ab7bac4a160f3eae40b77a5ec355f7e1cea4b47f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_94881cf39dd44fee19e6db2e79a9be90853207650b237a0159f8363a56a175dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94881cf39dd44fee19e6db2e79a9be90853207650b237a0159f8363a56a175dc->enter($__internal_94881cf39dd44fee19e6db2e79a9be90853207650b237a0159f8363a56a175dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Product creation</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\" />
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_94881cf39dd44fee19e6db2e79a9be90853207650b237a0159f8363a56a175dc->leave($__internal_94881cf39dd44fee19e6db2e79a9be90853207650b237a0159f8363a56a175dc_prof);

        
        $__internal_b1c7242b8bf916001495d392ab7bac4a160f3eae40b77a5ec355f7e1cea4b47f->leave($__internal_b1c7242b8bf916001495d392ab7bac4a160f3eae40b77a5ec355f7e1cea4b47f_prof);

    }

    // line 29
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ceddbaddce30fd1aab6d6beffae671745c97215b851de8b05eb2b0fedf609f58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ceddbaddce30fd1aab6d6beffae671745c97215b851de8b05eb2b0fedf609f58->enter($__internal_ceddbaddce30fd1aab6d6beffae671745c97215b851de8b05eb2b0fedf609f58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_0bf0f096c0e533db0e7a00979714d39d5fc55201f9c99ace56bee1b50f983af2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bf0f096c0e533db0e7a00979714d39d5fc55201f9c99ace56bee1b50f983af2->enter($__internal_0bf0f096c0e533db0e7a00979714d39d5fc55201f9c99ace56bee1b50f983af2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 30
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/newProductImg.js"), "html", null, true);
        echo "\">
";
        
        $__internal_0bf0f096c0e533db0e7a00979714d39d5fc55201f9c99ace56bee1b50f983af2->leave($__internal_0bf0f096c0e533db0e7a00979714d39d5fc55201f9c99ace56bee1b50f983af2_prof);

        
        $__internal_ceddbaddce30fd1aab6d6beffae671745c97215b851de8b05eb2b0fedf609f58->leave($__internal_ceddbaddce30fd1aab6d6beffae671745c97215b851de8b05eb2b0fedf609f58_prof);

    }

    public function getTemplateName()
    {
        return "products/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  95 => 29,  79 => 22,  74 => 20,  70 => 19,  56 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Product creation</h1>
          <a href=\"{{ path('products_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(form) }}
                {{ form_widget(form) }}
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\" />
            {{ form_end(form) }}
          </div>
        </div>
      </div>
    </main>
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\" src=\"{{asset('js/newProductImg.js')}}\">
{% endblock %}

    
", "products/new.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/products/new.html.twig");
    }
}
