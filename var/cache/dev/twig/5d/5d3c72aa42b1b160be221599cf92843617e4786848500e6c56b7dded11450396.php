<?php

/* users/new.html.twig */
class __TwigTemplate_22cc8c02f9f9a571f5168f8ab1ac39696a301a247893e618b051e8e4f638b17d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1bb9765fe070be123c0c5adb74f00c7849bc9c1f42c45719d7f3c3f982ba3e29 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bb9765fe070be123c0c5adb74f00c7849bc9c1f42c45719d7f3c3f982ba3e29->enter($__internal_1bb9765fe070be123c0c5adb74f00c7849bc9c1f42c45719d7f3c3f982ba3e29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/new.html.twig"));

        $__internal_0a174099376e10127ae799d9c57b13da71793bc6c5e9e4f0b802e4331ea715c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a174099376e10127ae799d9c57b13da71793bc6c5e9e4f0b802e4331ea715c9->enter($__internal_0a174099376e10127ae799d9c57b13da71793bc6c5e9e4f0b802e4331ea715c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1bb9765fe070be123c0c5adb74f00c7849bc9c1f42c45719d7f3c3f982ba3e29->leave($__internal_1bb9765fe070be123c0c5adb74f00c7849bc9c1f42c45719d7f3c3f982ba3e29_prof);

        
        $__internal_0a174099376e10127ae799d9c57b13da71793bc6c5e9e4f0b802e4331ea715c9->leave($__internal_0a174099376e10127ae799d9c57b13da71793bc6c5e9e4f0b802e4331ea715c9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9d8b65914613d0976b13f39062788f6054c82d5f3d0aa367a2a6684785eb1312 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d8b65914613d0976b13f39062788f6054c82d5f3d0aa367a2a6684785eb1312->enter($__internal_9d8b65914613d0976b13f39062788f6054c82d5f3d0aa367a2a6684785eb1312_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_09fc404266b1fda31bb2bf160059bd352fbc7d75c79abdfe8bea897b8128d08a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09fc404266b1fda31bb2bf160059bd352fbc7d75c79abdfe8bea897b8128d08a->enter($__internal_09fc404266b1fda31bb2bf160059bd352fbc7d75c79abdfe8bea897b8128d08a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> User creation</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_09fc404266b1fda31bb2bf160059bd352fbc7d75c79abdfe8bea897b8128d08a->leave($__internal_09fc404266b1fda31bb2bf160059bd352fbc7d75c79abdfe8bea897b8128d08a_prof);

        
        $__internal_9d8b65914613d0976b13f39062788f6054c82d5f3d0aa367a2a6684785eb1312->leave($__internal_9d8b65914613d0976b13f39062788f6054c82d5f3d0aa367a2a6684785eb1312_prof);

    }

    public function getTemplateName()
    {
        return "users/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 22,  73 => 20,  69 => 19,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> User creation</h1>
          <a href=\"{{ path('users_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(form) }}
                {{ form_widget(form) }}
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\"/>
            {{ form_end(form) }}
          </div>
        </div>
      </div>
    </main>
{% endblock %}
", "users/new.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/users/new.html.twig");
    }
}
