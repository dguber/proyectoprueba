<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_5ba2cb4b4871e706c5f44ec1fcf1a6201a6c2013df0f7b71e369a0c9b37ab7dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd99b4cc0f60e9c11b5f39ef0ac8ff28d27a39a82121999d1aee573e8f48fdf3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd99b4cc0f60e9c11b5f39ef0ac8ff28d27a39a82121999d1aee573e8f48fdf3->enter($__internal_bd99b4cc0f60e9c11b5f39ef0ac8ff28d27a39a82121999d1aee573e8f48fdf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_55f73f34f6ba86614dcf1c0d8e99638c5eb66558b0f4312feacefff6d90d5cb5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55f73f34f6ba86614dcf1c0d8e99638c5eb66558b0f4312feacefff6d90d5cb5->enter($__internal_55f73f34f6ba86614dcf1c0d8e99638c5eb66558b0f4312feacefff6d90d5cb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd99b4cc0f60e9c11b5f39ef0ac8ff28d27a39a82121999d1aee573e8f48fdf3->leave($__internal_bd99b4cc0f60e9c11b5f39ef0ac8ff28d27a39a82121999d1aee573e8f48fdf3_prof);

        
        $__internal_55f73f34f6ba86614dcf1c0d8e99638c5eb66558b0f4312feacefff6d90d5cb5->leave($__internal_55f73f34f6ba86614dcf1c0d8e99638c5eb66558b0f4312feacefff6d90d5cb5_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_75dd80aa2f66799fa0cc642be8ba6ca6f212e1a285c1ddc5bda18494b3381877 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75dd80aa2f66799fa0cc642be8ba6ca6f212e1a285c1ddc5bda18494b3381877->enter($__internal_75dd80aa2f66799fa0cc642be8ba6ca6f212e1a285c1ddc5bda18494b3381877_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_5ecfda04a5f42be8906a4bf25b55ebaaa12c1ee3434702cceb04fef234e41ab9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ecfda04a5f42be8906a4bf25b55ebaaa12c1ee3434702cceb04fef234e41ab9->enter($__internal_5ecfda04a5f42be8906a4bf25b55ebaaa12c1ee3434702cceb04fef234e41ab9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_5ecfda04a5f42be8906a4bf25b55ebaaa12c1ee3434702cceb04fef234e41ab9->leave($__internal_5ecfda04a5f42be8906a4bf25b55ebaaa12c1ee3434702cceb04fef234e41ab9_prof);

        
        $__internal_75dd80aa2f66799fa0cc642be8ba6ca6f212e1a285c1ddc5bda18494b3381877->leave($__internal_75dd80aa2f66799fa0cc642be8ba6ca6f212e1a285c1ddc5bda18494b3381877_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_28552148d47b4b4abb2c1fa32a7ad3d5ef18f0a04415fe566949d26430dab7b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28552148d47b4b4abb2c1fa32a7ad3d5ef18f0a04415fe566949d26430dab7b4->enter($__internal_28552148d47b4b4abb2c1fa32a7ad3d5ef18f0a04415fe566949d26430dab7b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1b7ab517406aa3d7bcc03b1fc5bfc525f236ae96be5371fda11056640dec696a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b7ab517406aa3d7bcc03b1fc5bfc525f236ae96be5371fda11056640dec696a->enter($__internal_1b7ab517406aa3d7bcc03b1fc5bfc525f236ae96be5371fda11056640dec696a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_1b7ab517406aa3d7bcc03b1fc5bfc525f236ae96be5371fda11056640dec696a->leave($__internal_1b7ab517406aa3d7bcc03b1fc5bfc525f236ae96be5371fda11056640dec696a_prof);

        
        $__internal_28552148d47b4b4abb2c1fa32a7ad3d5ef18f0a04415fe566949d26430dab7b4->leave($__internal_28552148d47b4b4abb2c1fa32a7ad3d5ef18f0a04415fe566949d26430dab7b4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "/home/usuario/Documentos/www/proyectoprueba/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
