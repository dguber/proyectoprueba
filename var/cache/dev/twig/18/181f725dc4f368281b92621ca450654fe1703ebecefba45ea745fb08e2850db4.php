<?php

/* subcategory/new.html.twig */
class __TwigTemplate_476493e9d125c718bf007300d69552ee326b005ef0e2b2e203dddafe226d24f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "subcategory/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fc6a7bba092017a7180eeb7c265bfc19d5e0658f023376f046342e462d06d14a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc6a7bba092017a7180eeb7c265bfc19d5e0658f023376f046342e462d06d14a->enter($__internal_fc6a7bba092017a7180eeb7c265bfc19d5e0658f023376f046342e462d06d14a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/new.html.twig"));

        $__internal_31895684a968fa68c820507447720581154c05b942d3f828605285fc6c58f91d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31895684a968fa68c820507447720581154c05b942d3f828605285fc6c58f91d->enter($__internal_31895684a968fa68c820507447720581154c05b942d3f828605285fc6c58f91d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "subcategory/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fc6a7bba092017a7180eeb7c265bfc19d5e0658f023376f046342e462d06d14a->leave($__internal_fc6a7bba092017a7180eeb7c265bfc19d5e0658f023376f046342e462d06d14a_prof);

        
        $__internal_31895684a968fa68c820507447720581154c05b942d3f828605285fc6c58f91d->leave($__internal_31895684a968fa68c820507447720581154c05b942d3f828605285fc6c58f91d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_fa9a9e27d265ed807c300d497392d6c3475aa8e4ccda111f6e4f7708b8bda1ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa9a9e27d265ed807c300d497392d6c3475aa8e4ccda111f6e4f7708b8bda1ad->enter($__internal_fa9a9e27d265ed807c300d497392d6c3475aa8e4ccda111f6e4f7708b8bda1ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3db3fe5e96f77ab1881808fbdc7d3dd4cdcdd570bad802f16bcacb31301c4677 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3db3fe5e96f77ab1881808fbdc7d3dd4cdcdd570bad802f16bcacb31301c4677->enter($__internal_3db3fe5e96f77ab1881808fbdc7d3dd4cdcdd570bad802f16bcacb31301c4677_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Sub-Category creation</h1>
          <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subcategory_index");
        echo "\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\"/>
            ";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </main>
";
        
        $__internal_3db3fe5e96f77ab1881808fbdc7d3dd4cdcdd570bad802f16bcacb31301c4677->leave($__internal_3db3fe5e96f77ab1881808fbdc7d3dd4cdcdd570bad802f16bcacb31301c4677_prof);

        
        $__internal_fa9a9e27d265ed807c300d497392d6c3475aa8e4ccda111f6e4f7708b8bda1ad->leave($__internal_fa9a9e27d265ed807c300d497392d6c3475aa8e4ccda111f6e4f7708b8bda1ad_prof);

    }

    // line 29
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e6a473886bab1d8b3af727e8fe8aac6d60fedb516e4e753d589ff0fc30d620df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6a473886bab1d8b3af727e8fe8aac6d60fedb516e4e753d589ff0fc30d620df->enter($__internal_e6a473886bab1d8b3af727e8fe8aac6d60fedb516e4e753d589ff0fc30d620df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_8f06f96743624ef662bc3dd7c45616024d5bae601c8629db94b4f84e9e917917 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f06f96743624ef662bc3dd7c45616024d5bae601c8629db94b4f84e9e917917->enter($__internal_8f06f96743624ef662bc3dd7c45616024d5bae601c8629db94b4f84e9e917917_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 30
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/newProductImg.js"), "html", null, true);
        echo "\">
";
        
        $__internal_8f06f96743624ef662bc3dd7c45616024d5bae601c8629db94b4f84e9e917917->leave($__internal_8f06f96743624ef662bc3dd7c45616024d5bae601c8629db94b4f84e9e917917_prof);

        
        $__internal_e6a473886bab1d8b3af727e8fe8aac6d60fedb516e4e753d589ff0fc30d620df->leave($__internal_e6a473886bab1d8b3af727e8fe8aac6d60fedb516e4e753d589ff0fc30d620df_prof);

    }

    public function getTemplateName()
    {
        return "subcategory/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  95 => 29,  79 => 22,  74 => 20,  70 => 19,  56 => 8,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
      <div class=\"app-title\">
        <div>
          <h1><i class=\"fa fa-edit\"></i> Sub-Category creation</h1>
          <a href=\"{{ path('subcategory_index') }}\">Back to the list</a>
        </div>
        <ul class=\"app-breadcrumb breadcrumb\">
          <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
          <li class=\"breadcrumb-item\">Forms</li>
          <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
        </ul>
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"bs-component\">
            {{ form_start(form) }}
                {{ form_widget(form) }}
                <input type=\"submit\" value=\"Create\" class=\"btn btn-primary\"/>
            {{ form_end(form) }}
          </div>
        </div>
      </div>
    </main>
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\" src=\"{{asset('js/newProductImg.js')}}\">
{% endblock %}
", "subcategory/new.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/subcategory/new.html.twig");
    }
}
