<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_078ed254cddf017d78c221de61bcd7acb7fd61457406bb1a7dc2d56e5142c97b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b67e046c6672fd4fe3ed9c359d7121ee2ed49b571b92af590edee806cc71e77a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b67e046c6672fd4fe3ed9c359d7121ee2ed49b571b92af590edee806cc71e77a->enter($__internal_b67e046c6672fd4fe3ed9c359d7121ee2ed49b571b92af590edee806cc71e77a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_d1edfb194f930dddda67880a13ae189331fb27ecc31d52a6d803c0d53fcf1429 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1edfb194f930dddda67880a13ae189331fb27ecc31d52a6d803c0d53fcf1429->enter($__internal_d1edfb194f930dddda67880a13ae189331fb27ecc31d52a6d803c0d53fcf1429_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b67e046c6672fd4fe3ed9c359d7121ee2ed49b571b92af590edee806cc71e77a->leave($__internal_b67e046c6672fd4fe3ed9c359d7121ee2ed49b571b92af590edee806cc71e77a_prof);

        
        $__internal_d1edfb194f930dddda67880a13ae189331fb27ecc31d52a6d803c0d53fcf1429->leave($__internal_d1edfb194f930dddda67880a13ae189331fb27ecc31d52a6d803c0d53fcf1429_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_09b5b57ed4898a02018a1ee5f933d38fe23abc8259a16c16b908f23615a01183 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09b5b57ed4898a02018a1ee5f933d38fe23abc8259a16c16b908f23615a01183->enter($__internal_09b5b57ed4898a02018a1ee5f933d38fe23abc8259a16c16b908f23615a01183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9e455875b4be89e115bd35bcbe6e266414147fbfcc8c9df6623f7501a1c7d4ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e455875b4be89e115bd35bcbe6e266414147fbfcc8c9df6623f7501a1c7d4ad->enter($__internal_9e455875b4be89e115bd35bcbe6e266414147fbfcc8c9df6623f7501a1c7d4ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9e455875b4be89e115bd35bcbe6e266414147fbfcc8c9df6623f7501a1c7d4ad->leave($__internal_9e455875b4be89e115bd35bcbe6e266414147fbfcc8c9df6623f7501a1c7d4ad_prof);

        
        $__internal_09b5b57ed4898a02018a1ee5f933d38fe23abc8259a16c16b908f23615a01183->leave($__internal_09b5b57ed4898a02018a1ee5f933d38fe23abc8259a16c16b908f23615a01183_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ef46f715d3f2cb9a09fc7bac23038bee0a0e53416760ee7033529c7e6b2eb2ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef46f715d3f2cb9a09fc7bac23038bee0a0e53416760ee7033529c7e6b2eb2ce->enter($__internal_ef46f715d3f2cb9a09fc7bac23038bee0a0e53416760ee7033529c7e6b2eb2ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_b6e121b95dc774b9d3707b35564b715ee9209e9c816778e385d23ee04fcf6554 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6e121b95dc774b9d3707b35564b715ee9209e9c816778e385d23ee04fcf6554->enter($__internal_b6e121b95dc774b9d3707b35564b715ee9209e9c816778e385d23ee04fcf6554_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_b6e121b95dc774b9d3707b35564b715ee9209e9c816778e385d23ee04fcf6554->leave($__internal_b6e121b95dc774b9d3707b35564b715ee9209e9c816778e385d23ee04fcf6554_prof);

        
        $__internal_ef46f715d3f2cb9a09fc7bac23038bee0a0e53416760ee7033529c7e6b2eb2ce->leave($__internal_ef46f715d3f2cb9a09fc7bac23038bee0a0e53416760ee7033529c7e6b2eb2ce_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_83f2078a6a8ec360df6fc3e5fe7406ada0a1ac43e6413038249298e72ca87859 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83f2078a6a8ec360df6fc3e5fe7406ada0a1ac43e6413038249298e72ca87859->enter($__internal_83f2078a6a8ec360df6fc3e5fe7406ada0a1ac43e6413038249298e72ca87859_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_4d86c214825a0aed53bcc971ae80d5396a840624146f30b06894052274be27e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d86c214825a0aed53bcc971ae80d5396a840624146f30b06894052274be27e7->enter($__internal_4d86c214825a0aed53bcc971ae80d5396a840624146f30b06894052274be27e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_4d86c214825a0aed53bcc971ae80d5396a840624146f30b06894052274be27e7->leave($__internal_4d86c214825a0aed53bcc971ae80d5396a840624146f30b06894052274be27e7_prof);

        
        $__internal_83f2078a6a8ec360df6fc3e5fe7406ada0a1ac43e6413038249298e72ca87859->leave($__internal_83f2078a6a8ec360df6fc3e5fe7406ada0a1ac43e6413038249298e72ca87859_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/usuario/Documentos/www/proyectoprueba/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
