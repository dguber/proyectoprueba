<?php

/* base.html.twig */
class __TwigTemplate_56a892609981b9a4569575c7ed45bce78650b1c7e450419f0e64b58c0534223e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3179e0e54eb162717bed80a08f74339fdf936384e240ced6364866bd1ac027e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3179e0e54eb162717bed80a08f74339fdf936384e240ced6364866bd1ac027e->enter($__internal_c3179e0e54eb162717bed80a08f74339fdf936384e240ced6364866bd1ac027e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_b3e9af8133aca6baba13891d59a94d6aec8f7d164d90826d6230626591b64ccd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3e9af8133aca6baba13891d59a94d6aec8f7d164d90826d6230626591b64ccd->enter($__internal_b3e9af8133aca6baba13891d59a94d6aec8f7d164d90826d6230626591b64ccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta name=\"description\" content=\"Tienda virtual en Symfony\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <meta charset=\"UTF-8\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <!-- Main CSS-->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/"), "html", null, true);
        echo "main.css\">
        <!-- Font-icon css-->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body class=\"app sidebar-mini\">
        ";
        // line 17
        $this->loadTemplate("navbar.html.twig", "base.html.twig", 17)->display(array());
        // line 18
        echo "        ";
        $this->loadTemplate("sidebar.html.twig", "base.html.twig", 18)->display(array());
        // line 19
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "        ";
        $this->loadTemplate("extra_scripts.html.twig", "base.html.twig", 21)->display(array());
        // line 22
        echo "    </body>

</html>
";
        
        $__internal_c3179e0e54eb162717bed80a08f74339fdf936384e240ced6364866bd1ac027e->leave($__internal_c3179e0e54eb162717bed80a08f74339fdf936384e240ced6364866bd1ac027e_prof);

        
        $__internal_b3e9af8133aca6baba13891d59a94d6aec8f7d164d90826d6230626591b64ccd->leave($__internal_b3e9af8133aca6baba13891d59a94d6aec8f7d164d90826d6230626591b64ccd_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a079d3e01709bbc1aeed367ffc40c1af96f288a33f68c3bb550ded70496f1ebb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a079d3e01709bbc1aeed367ffc40c1af96f288a33f68c3bb550ded70496f1ebb->enter($__internal_a079d3e01709bbc1aeed367ffc40c1af96f288a33f68c3bb550ded70496f1ebb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c74cd42874abca3d676720677b0352c51c5e8a82023a05107367d0c990051eb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c74cd42874abca3d676720677b0352c51c5e8a82023a05107367d0c990051eb1->enter($__internal_c74cd42874abca3d676720677b0352c51c5e8a82023a05107367d0c990051eb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Tienda Virtual";
        
        $__internal_c74cd42874abca3d676720677b0352c51c5e8a82023a05107367d0c990051eb1->leave($__internal_c74cd42874abca3d676720677b0352c51c5e8a82023a05107367d0c990051eb1_prof);

        
        $__internal_a079d3e01709bbc1aeed367ffc40c1af96f288a33f68c3bb550ded70496f1ebb->leave($__internal_a079d3e01709bbc1aeed367ffc40c1af96f288a33f68c3bb550ded70496f1ebb_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_728feecddc389045c3c4d56b51babad09123cd48290c3c6d10a9de55fd9b46aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_728feecddc389045c3c4d56b51babad09123cd48290c3c6d10a9de55fd9b46aa->enter($__internal_728feecddc389045c3c4d56b51babad09123cd48290c3c6d10a9de55fd9b46aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_d9a84326c2fa7beabbfed0e0321ddda51be26a95f156862b6da6b3afaafa6f23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9a84326c2fa7beabbfed0e0321ddda51be26a95f156862b6da6b3afaafa6f23->enter($__internal_d9a84326c2fa7beabbfed0e0321ddda51be26a95f156862b6da6b3afaafa6f23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_d9a84326c2fa7beabbfed0e0321ddda51be26a95f156862b6da6b3afaafa6f23->leave($__internal_d9a84326c2fa7beabbfed0e0321ddda51be26a95f156862b6da6b3afaafa6f23_prof);

        
        $__internal_728feecddc389045c3c4d56b51babad09123cd48290c3c6d10a9de55fd9b46aa->leave($__internal_728feecddc389045c3c4d56b51babad09123cd48290c3c6d10a9de55fd9b46aa_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_5b70aa7c983222df3ebb1d9b175da6613e5f21c938fa9c80924a09f58a8988a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b70aa7c983222df3ebb1d9b175da6613e5f21c938fa9c80924a09f58a8988a7->enter($__internal_5b70aa7c983222df3ebb1d9b175da6613e5f21c938fa9c80924a09f58a8988a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b5a65a82c38fa339a902d9d366087ab5d332cef89ceb5406278f9db47c4afae5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5a65a82c38fa339a902d9d366087ab5d332cef89ceb5406278f9db47c4afae5->enter($__internal_b5a65a82c38fa339a902d9d366087ab5d332cef89ceb5406278f9db47c4afae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b5a65a82c38fa339a902d9d366087ab5d332cef89ceb5406278f9db47c4afae5->leave($__internal_b5a65a82c38fa339a902d9d366087ab5d332cef89ceb5406278f9db47c4afae5_prof);

        
        $__internal_5b70aa7c983222df3ebb1d9b175da6613e5f21c938fa9c80924a09f58a8988a7->leave($__internal_5b70aa7c983222df3ebb1d9b175da6613e5f21c938fa9c80924a09f58a8988a7_prof);

    }

    // line 20
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d6d069f2249f0d54c442488610908246e359b73e1726f42248cb040a1d68c7ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6d069f2249f0d54c442488610908246e359b73e1726f42248cb040a1d68c7ee->enter($__internal_d6d069f2249f0d54c442488610908246e359b73e1726f42248cb040a1d68c7ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d8593505f38e2c6eb9c132d47116d769194a6000353de775176dd0585fe4b4e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8593505f38e2c6eb9c132d47116d769194a6000353de775176dd0585fe4b4e0->enter($__internal_d8593505f38e2c6eb9c132d47116d769194a6000353de775176dd0585fe4b4e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_d8593505f38e2c6eb9c132d47116d769194a6000353de775176dd0585fe4b4e0->leave($__internal_d8593505f38e2c6eb9c132d47116d769194a6000353de775176dd0585fe4b4e0_prof);

        
        $__internal_d6d069f2249f0d54c442488610908246e359b73e1726f42248cb040a1d68c7ee->leave($__internal_d6d069f2249f0d54c442488610908246e359b73e1726f42248cb040a1d68c7ee_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 20,  121 => 19,  104 => 6,  86 => 5,  73 => 22,  70 => 21,  67 => 20,  64 => 19,  61 => 18,  59 => 17,  53 => 14,  47 => 11,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta name=\"description\" content=\"Tienda virtual en Symfony\">
        <title>{% block title %}Tienda Virtual{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <meta charset=\"UTF-8\" />
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <!-- Main CSS-->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/') }}main.css\">
        <!-- Font-icon css-->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body class=\"app sidebar-mini\">
        {% include 'navbar.html.twig' only %}
        {% include 'sidebar.html.twig' only %}
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
        {% include 'extra_scripts.html.twig' only %}
    </body>

</html>
", "base.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/base.html.twig");
    }
}
