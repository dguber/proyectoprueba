<?php

/* users/show.html.twig */
class __TwigTemplate_86efbb0b1435afafdbb699cbddc896c64a5ed8431184b875b53919c6fb211d53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "users/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6534367230aa2fa6778a41d5175d980c7ea4db8d475575d56df82576e7c2cc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6534367230aa2fa6778a41d5175d980c7ea4db8d475575d56df82576e7c2cc3->enter($__internal_b6534367230aa2fa6778a41d5175d980c7ea4db8d475575d56df82576e7c2cc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/show.html.twig"));

        $__internal_ce842819c99a78a813d305efcf617f11573d051fa0004e399ee3bbe9fcd6f3a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce842819c99a78a813d305efcf617f11573d051fa0004e399ee3bbe9fcd6f3a7->enter($__internal_ce842819c99a78a813d305efcf617f11573d051fa0004e399ee3bbe9fcd6f3a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b6534367230aa2fa6778a41d5175d980c7ea4db8d475575d56df82576e7c2cc3->leave($__internal_b6534367230aa2fa6778a41d5175d980c7ea4db8d475575d56df82576e7c2cc3_prof);

        
        $__internal_ce842819c99a78a813d305efcf617f11573d051fa0004e399ee3bbe9fcd6f3a7->leave($__internal_ce842819c99a78a813d305efcf617f11573d051fa0004e399ee3bbe9fcd6f3a7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_aa6b556a4373b9f1a42480d654051b237e34e6d6b0b67cb19b032c4158c76968 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa6b556a4373b9f1a42480d654051b237e34e6d6b0b67cb19b032c4158c76968->enter($__internal_aa6b556a4373b9f1a42480d654051b237e34e6d6b0b67cb19b032c4158c76968_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1af9fb5e21c30473b633389974776b0a5fa653021679efafccf757fa1725b129 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1af9fb5e21c30473b633389974776b0a5fa653021679efafccf757fa1725b129->enter($__internal_1af9fb5e21c30473b633389974776b0a5fa653021679efafccf757fa1725b129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> User</h1>
            <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Category: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "password", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Email: ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        <li>
                            <a href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("users_edit", array("id" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
                        </li>
                        <li>
                            ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
                        </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
";
        
        $__internal_1af9fb5e21c30473b633389974776b0a5fa653021679efafccf757fa1725b129->leave($__internal_1af9fb5e21c30473b633389974776b0a5fa653021679efafccf757fa1725b129_prof);

        
        $__internal_aa6b556a4373b9f1a42480d654051b237e34e6d6b0b67cb19b032c4158c76968->leave($__internal_aa6b556a4373b9f1a42480d654051b237e34e6d6b0b67cb19b032c4158c76968_prof);

    }

    public function getTemplateName()
    {
        return "users/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 32,  95 => 30,  89 => 27,  82 => 23,  78 => 22,  74 => 21,  70 => 20,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <main class=\"app-content\">
        <div class=\"app-title\">
          <div>
            <h1><i class=\"fa fa-edit\"></i> User</h1>
            <a href=\"{{ path('products_index') }}\">Back to the list</a>
          </div>
          <ul class=\"app-breadcrumb breadcrumb\">
            <li class=\"breadcrumb-item\"><i class=\"fa fa-home fa-lg\"></i></li>
            <li class=\"breadcrumb-item\">Forms</li>
            <li class=\"breadcrumb-item\"><a href=\"#\">Sample Forms</a></li>
          </ul>
        </div>
        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"bs-component\">
               <ul class=\"list-group\">
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">ID: {{ user.id }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Name: {{ user.username }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Category: {{ user.password }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">Email: {{ user.email }}</li>
                <li class=\"list-group-item\"><span class=\"tag tag-default tag-pill float-xs-right\">
                    <ul>
                        <li>
                            <a href=\"{{ path('users_edit', { 'id': user.id }) }}\">Edit</a>
                        </li>
                        <li>
                            {{ form_start(delete_form) }}
                                <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\">
                            {{ form_end(delete_form) }}
                        </li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </main>
{% endblock %}
", "users/show.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/users/show.html.twig");
    }
}
