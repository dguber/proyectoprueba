<?php

/* sidebar.html.twig */
class __TwigTemplate_8b9e5a8dd3879706b6c4d824b701699fa9090872b8fcbaae28ba3af4cea422b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <!-- Sidebar menu-->
    <div class=\"app-sidebar__overlay\" data-toggle=\"sidebar\"></div>
    <aside class=\"app-sidebar\">
      <div class=\"app-sidebar__user\"><img class=\"app-sidebar__user-avatar\" src=\"https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg\" alt=\"User Image\">
        <div>
          <p class=\"app-sidebar__user-name\">John Doe</p>
          <p class=\"app-sidebar__user-designation\">Frontend Developer</p>
        </div>
      </div>
      <ul class=\"app-menu\">
        <li><a class=\"app-menu__item\" href=\"\"><i class=\"app-menu__icon fa fa-dashboard\"></i><span class=\"app-menu__label\">Home</span></a></li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"#\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-th-list\"></i><span class=\"app-menu__label\">Products</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu is-expanded\">
            <li><a class=\"treeview-item\" href=\"products/\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item active\" href=\"products/new\"><i class=\"icon fa fa-circle-o\"></i> New product</a></li>
          </ul>
        </li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"#\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-edit\"></i><span class=\"app-menu__label\">Categories</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"categories/\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item\" href=\"categories/new\"><i class=\"icon fa fa-circle-o\"></i> New category</a></li>
          </ul>
        </li>
        <li class=\"treeview\"><a class=\"app-menu__item\" href=\"#\" data-toggle=\"treeview\"><i class=\"app-menu__icon fa fa-th-list\"></i><span class=\"app-menu__label\">Users</span><i class=\"treeview-indicator fa fa-angle-right\"></i></a>
          <ul class=\"treeview-menu\">
            <li><a class=\"treeview-item\" href=\"users/\"><i class=\"icon fa fa-circle-o\"></i> List</a></li>
            <li><a class=\"treeview-item\" href=\"users/new\"><i class=\"icon fa fa-circle-o\"></i> New user</a></li>
          </ul>
        </li>
      </ul>
    </aside>";
    }

    public function getTemplateName()
    {
        return "sidebar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "sidebar.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/sidebar.html.twig");
    }
}
