<?php

/* security/login.html.twig */
class __TwigTemplate_285041d10a54a149ad5522132b6a5642e6ba5f14734150909e8ff420854ecd84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- Main CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">
    <!-- Font-icon css-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <title>Login - Vali Admin</title>
  </head>
  ";
        // line 13
        $this->displayBlock('body', $context, $blocks);
        // line 61
        echo "</html>";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "  <body>
    <section class=\"material-half-bg\">
      <div class=\"cover\"></div>
    </section>
    <section class=\"login-content\">
      <div class=\"logo\">
        <h1>Vali</h1>
      </div>
      <div class=\"login-box\">
        <form class=\"login-form\" action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
          <h3 class=\"login-head\"><i class=\"fa fa-lg fa-fw fa-user\"></i>Sign in</h3>
          <div class=\"form-group\">
            <label class=\"control-label\">USERNAME</label>
            <input class=\"form-control\" type=\"text\" placeholder=\"Username\" id=\"username\" name=\"_username\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" autofocus>
          </div>
          <div class=\"form-group\">
            <label class=\"control-label\">PASSWORD</label>
            <input class=\"form-control\" type=\"password\" placeholder=\"Password\" id=\"password\" name=\"_password\">
            <input type=\"hidden\" name=\"go_to\" value=\"/products\" />
          </div>
          <div class=\"form-group\">
            ";
        // line 35
        if (($context["error"] ?? null)) {
            // line 36
            echo "                <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? null), "messageKey", array()), $this->getAttribute(($context["error"] ?? null), "messageData", array()), "security"), "html", null, true);
            echo "</div>
            ";
        }
        // line 38
        echo "          </div>
          <div class=\"form-group btn-container\">
            <button class=\"btn btn-primary btn-block\" type=\"submit\"><i class=\"fa fa-sign-in fa-lg fa-fw\"></i>Sign in</button>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src=\"js/jquery-3.3.1.min.js\"></script>
    <script src=\"js/popper.min.js\"></script>
    <script src=\"js/bootstrap.min.js\"></script>
    <script src=\"js/main.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"js/plugins/pace.min.js\"></script>
    <script type=\"text/javascript\">
      // Login Page Flipbox control
      \$('.login-content [data-toggle=\"flip\"]').click(function() {
        \$('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
  ";
    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  80 => 38,  74 => 36,  72 => 35,  61 => 27,  54 => 23,  43 => 14,  40 => 13,  36 => 61,  34 => 13,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "security/login.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/security/login.html.twig");
    }
}
