<?php

/* extra_scripts.html.twig */
class __TwigTemplate_7e6b775431bdb6f08d027797784e47223ff19bb0f31c1041251f558fe0313ecf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <!-- Essential javascripts for application to work-->
    <script src=\"../js/jquery-3.3.1.min.js\"></script>
    <script src=\"../js/popper.min.js\"></script>
    <script src=\"../js/bootstrap.min.js\"></script>
    <script src=\"../js/main.js\"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src=\"../js/plugins/pace.min.js\"></script>
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type=\"text/javascript\" src=\"../js/plugins/jquery.dataTables.min.js\"></script>
    <script type=\"text/javascript\" src=\"../js/plugins/dataTables.bootstrap.min.js\"></script>
    <script type=\"text/javascript\">\$('#sampleTable').DataTable();</script>
    <!-- Google analytics script-->
    <script type=\"text/javascript\">
      if(document.location.hostname == 'pratikborsadiya.in') {
      \t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      \t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      \tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      \t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      \tga('create', 'UA-72504830-1', 'auto');
      \tga('send', 'pageview');
      }
    </script>";
    }

    public function getTemplateName()
    {
        return "extra_scripts.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "extra_scripts.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/extra_scripts.html.twig");
    }
}
