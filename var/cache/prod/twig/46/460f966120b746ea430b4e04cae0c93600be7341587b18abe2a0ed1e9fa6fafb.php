<?php

/* navbar.html.twig */
class __TwigTemplate_3d7d659d0de84f60e19a587d3665a93b681136f61eab9047fb6274c3bedb966a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'usermenu' => array($this, 'block_usermenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <!-- Navbar-->
    <header class=\"app-header\"><a class=\"app-header__logo\" href=\"../\">Tienda Virtual</a>
      <!-- Sidebar toggle button-->
      <a class=\"app-sidebar__toggle\" href=\"#\" data-toggle=\"sidebar\" aria-label=\"Hide Sidebar\"></a>
      <!-- Navbar Right Menu-->
      <ul class=\"app-nav\">
        <!-- User Menu-->
        ";
        // line 8
        $this->displayBlock('usermenu', $context, $blocks);
        // line 21
        echo "      </ul>
    </header>";
    }

    // line 8
    public function block_usermenu($context, array $blocks = array())
    {
        // line 9
        echo "        <li class=\"dropdown\"><a class=\"app-nav__item\" href=\"#\" data-toggle=\"dropdown\" aria-label=\"Open Profile Menu\"><i class=\"fa fa-user fa-lg\"></i></a>
          <ul class=\"dropdown-menu settings-menu dropdown-menu-right\">
            ";
        // line 11
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 12
            echo "            <li><a class=\"dropdown-item\" href=\"users/\"><i class=\"fa fa-cog fa-lg\"></i> Settings</a></li>
            <li><a class=\"dropdown-item\" href=\"users/\"><i class=\"fa fa-user fa-lg\"></i> Profile</a></li>
            <li><a class=\"dropdown-item\" href=\"/logout\"><i class=\"fa fa-sign-out fa-lg\"></i> Logout</a></li>
            ";
        } else {
            // line 16
            echo "            <li><a class=\"dropdown-item\" href=\"/login\"><i class=\"fa fa-sign-out fa-lg\"></i> Login</a></li>
            ";
        }
        // line 18
        echo "          </ul>
        </li>
        ";
    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  55 => 18,  51 => 16,  45 => 12,  43 => 11,  39 => 9,  36 => 8,  31 => 21,  29 => 8,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "navbar.html.twig", "/home/usuario/Documentos/www/proyectoprueba/app/Resources/views/navbar.html.twig");
    }
}
