<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use AppBundle\Entity\Subcategory;

use Doctrine\ORM\EntityRepository;
//use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;




class ProductsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('name')
                 ->add('price')

                 ->add('category', EntityType::class,
                    [
                         'class'        => \AppBundle\Entity\Categories::class,
                         'placeholder'  => 'Select a category',
                         'choice_label' => function($category) 
                         {
                            //return "{$category->getId()} - {$category->getName()}";
                            return "{$category->getName()}";
                         },
                    ]
                 )
                 ->add('subcategory', EntityType::class,
                    [
                         'class'        => \AppBundle\Entity\Subcategory::class,
                         'placeholder'  => 'Select a subcategory',
                         'choice_label' => function($subcategory) 
                         {
                            //return "{$category->getId()} - {$category->getName()}";
                            return "{$subcategory->getName()}";
                         },
                    ]
                 )
                 /*->add('image', 
                    FileType::class,array(
                        'label'         => 'Imagen:',
                        'attr'          => array('onchange' => 'onChange(event)'),
                        'data_class'    => null
                    )
                 )*/;
            
        /*$builder->get('category')->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event)
            {
                $form = $event->getForm();
                $data = $event->getData();

                $form->getParent()->add('subcategory', EntityType::class, [
                    
                    'class'         => \AppBundle\Entity\Subcategory::class,
                    'placeholder'   => 'Select a subcategory',
                    'query_builder' => function(EntityRepository $er) use ($data)
                         {
                             return $er->createQueryBuilder()
                                ->select('s')
                                ->from('subcategory', 's', 's.name')
                                ->where('s.categoryId = ?1')
                                ->orderBy('s.name', 'ASC')
                                ->setParameter(1, $data);
                         },
                    'choice_label'  => 'name',
                ]);
            }
        );*/
            
        $builder ->add('image', 
                     FileType::class,array(
                         'label'         => 'Imagen:',
                         'attr'          => array('onchange' => 'onChange(event)'),
                         'data_class'    => null
                     )
                 );
    }
    
    /**
     * {@inheritdoc}
     */
    public function searchForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('name');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Products'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_products';
    }


}
