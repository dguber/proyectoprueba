<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Categories;
use Doctrine\ORM\EntityRepository;

class SubcategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('name')
        
                 ->add('icon', FileType::class,array(
                        'label'         => 'Imagen:',
                        'attr'          => array('onchange' => 'onChange(event)'),
                        'data_class'    => null
                    )
                 )

                 ->add('categoryId', EntityType::class,
                    [
                         'class'            => \AppBundle\Entity\Categories::class,
                         'choice_label' => function($category) 
                         {
                            //return "{$category->getId()} - {$category->getName()}";
                            $categories = $this->getDoctrine()->getRepository(Categories::class)->getCategories();
                            return "{$category->getName()}";
                         },
                    ]
                 );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Subcategory'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_subcategory';
    }


}
