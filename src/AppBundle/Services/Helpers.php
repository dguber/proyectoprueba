<?php
namespace AppBundle\Services;
 
class Helpers {
 
    public function uploadImage($file, $type = 0)
    {
     	// Sacamos la extensión del fichero
        $ext=$file->guessExtension();
             
        // Le ponemos un nombre al fichero
        $file_name=$this->generateUniqueFileName().".".$ext;
           
        // Guardamos el fichero en el directorio uploads que estará en el directorio /web del framework
        if($type == 1) //products
        	$file->move("img/products", $file_name);
        elseif($type == 2)
        	$file->move("img/categories", $file_name);
        elseif($type == 3)
        	$file->move("img/subcategories", $file_name);
        else
        	$file->move("img/", $file_name);
        
        if($file_name)
        {
        	return $file_name;
        }
        else
        {
        	return false;
        }
    }

    /**
    * @return string
    */

    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}
