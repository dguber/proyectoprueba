<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Products;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Product controller.
 *
 * @Route("products")
 */
class ProductsController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/", name="products_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //Parte del buscador

        //$product = new Products();
        $form = $this->createForm('AppBundle\Form\sProductsType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dump($form->getData());
            //die();
            $em = $this->getDoctrine()->getManager();

            $products = $em->getRepository('AppBundle:Products')->getProducts($form->getData()->getName());

            return $this->render('products/index.html.twig', array(
            'products'  => $products,
            'form'      => $form->createView()
            ));
        }
        else
        {
            $products = $em->getRepository('AppBundle:Products')->getProducts();

            return $this->render('products/index.html.twig', array(
                'products'  => $products,
                'form'      => $form->createView()
            ));
        }

        //hasta aca buscador
    }

    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="products_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction(Request $request)
    {
        $product = new Products();
        $form = $this->createForm('AppBundle\Form\ProductsType', $product);
        $form->handleRequest($request);
        


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Aca iria la parte de guardar la imagen.

            // Recogemos el fichero
            $file=$form['image']->getData();

            if($file)
            {
                $file_name = $this->get("app.helpers")->uploadImage($file,1);
                $product->setImage($file_name);
            }
            else
            {
                $file_name = $product->getImage();
                $product->setImage($file_name);
            }
            
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('products_show', array('id' => $product->getId()));
        }

        return $this->render('products/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="products_show")
     * @Method("GET")
     */
    public function showAction(Products $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('products/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="products_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, Products $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        $oldFile = $product->getImage();
        $editForm = $this->createForm('AppBundle\Form\ProductsType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()/*->flush()*/;

            // Recogemos el fichero
             
            $file=$editForm['image']->getData();

            if($file)
            {
                unlink("img/products/".$oldFile);
                $file_name = $this->get("app.helpers")->uploadImage($file,1);
                $product->setImage($file_name);
            }
            else
            {
                $file_name = $product->getImage();
                $product->setImage($file_name);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('products_edit', array('id' => $product->getId()));
        }

        return $this->render('products/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("/{id}", name="products_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, Products $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // Ver de hacer delete image
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('products_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Products $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Products $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('products_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
    * @return string
    */

    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}
