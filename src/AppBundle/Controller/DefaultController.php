<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Categories;
use AppBundle\Entity\Products;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /*public function createAction()
    {
        $category = new Category();
        $category->setName('Peripherals');
        $category->setIcon('/home/User/Images/peripheral.png');

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($category);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return new Response('Saved new category with id '.$category->getId());
    }

    public function createAction2()
    {
        $product = new Product();
        $product->setName('Keyboard');
        $product->setCategory(1);
        $product->setImage('/home/User/Images/Keyboard.png');

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return new Response('Saved new product with id '.$product->getId());
    }*/
}


