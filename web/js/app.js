var $product_category = $("#appbundle_products_category")
var $token = $("#appbundle_products__token")

$product_category.change(function()
{
	var $form = $(this).closest('form')

	var data = {}

	data[$token.attr('name')] = $token.val()
	data[$product_category.attr(name)] = $product_category.val()

	$.post($form.attr('action'), data).then(function (response)
	{
		$("#appbundle_products_subcategory").replaceWith(
			$(response).find("#appbundle_products_subcategory")
		)
	})
})



